# WriteUp Word add-in README #

This add-in will let you perform following tasks right from within the Word:

* login to WriteUp using your credentials
* check your previously set up projects
* see documents list for selected project
* read through selected document content
* insert selected document text as quote, reference or both to Word document
* navigate back to selected document text by clicking on previously inserted reference within Word document 
* type in text with autocomplete and shortcuts and send to Word document

### What is this repository for? ###

* This repository contains Word add-in sorouces only
* This is MVP version (0.0.6.0, check XML manifest for details)

### Prerequisites and troubleshooting ###

* Add-in relies on both *Shared JavaScript API for Office* introduced in Office 2013 and strongly typed *Word JavaScript API* introduced in Office 2016
* Add-in tested in Office for Windows 2016 and Office for Mac 15 (Office 365 subscription)
* Add-in on Mac is not supported at this time due to the [manifest reload issue](http://stackoverflow.com/questions/39636767/how-to-force-word-for-mac-15-26-to-read-add-in-manifest-xml/39650255?noredirect=1#comment66806277_39650255) (may be resolved using suggested workarounds)
* Both Word Office 365 and JS API are using evergreen update cycle, so both are subject to change at any time (which may introduce unexpected API and platform behavior)
* You may find dedicated Q\A on Stackoverflow.com using one of the following tags: [ms-word], [ms-office], [office365], [office-js]

### How do I get set up? ###

* Set up
    * Install Git using defaults. [Git downloads](https://git-scm.com/downloads)
    * Install npm using defaults. [Installing npm](https://howtonode.org/introduction-to-npm)
    * Clone this repository and switch to root project folder. [Create and clone a repository](https://confluence.atlassian.com/bitbucket/create-and-clone-a-repository-800695642.html)
    * Install gulp globally. [Gulp Getting Started](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
    * Build for distribution using `gulp dist` command
    * Check build results in `dist` folder
* Configuration
    * Currently add-in hosted on Firebase. You may refer to it by the following URL: https://retain-word-addin.firebaseapp.com/app/home/home.html
    * MVP version of the add-in uses API by: https://retain.demo.relevant.software/v1
    * API documentation: http://apidoc.retain.demo.relevant.software/
    * [Admin site](https://retain.demo.relevant.software/auth/default/login)
    * [Firebase project user admin panel](https://console.firebase.google.com/iam-admin/iam/project?project=retain-word-addin&consoleReturnUrl=https:%2F%2Fconsole.firebase.google.com%2Fproject%2Fretain-word-addin%2Fsettings%2Fgeneral%2F)
    * [Firebase project console](https://console.firebase.google.com/project/retain-word-addin/settings/general/)
* Dependencies
    * [Office UI Fabric 2.6.1](https://github.com/OfficeDev/office-ui-fabric-core/releases/tag/2.6.1)
    * [jQuery 3.0.0](https://jquery.com/)
    * [Bootstrap Datetimepicker 4.17](https://github.com/Eonasdan/bootstrap-datetimepicker/)
    * [Bootstrap jQuery Page plugin 1.0.0](https://vkelembet.github.io/bootstrap-jquery-page)
    * [Bootstrap Popover plugin extension](https://vkelembet.github.io/bootstrap-popover-extended/)
    * [Handlebars 4.0.5](http://handlebarsjs.com/installation.html)
    * [jQuery Textcomplete plugin](https://github.com/yuku-t/jquery-textcomplete)
    * [jQuery Highlighter plugin](https://bitbucket.org/vkelembet/retain-highlight-widget)
    * [pdf2htmlEX](http://coolwanglu.github.io/pdf2htmlEX/)
* Deployment instructions
    * Install Firebase CLI if not installed. [Firebase deploying](https://firebase.google.com/docs/hosting/deploying)
    * Switch to the project root ant initialize (if not initialized previously) using `firebase init` command
    * Run distribution build using `gulp dist`
    * Deploy dist folder to Firebase hosting using `firebase deploy`
* Run add-in in Word (Office 356) for testing
    * [Install latest Office version](https://dev.office.com/docs/add-ins/develop/install-latest-office-version)
    * [Sideload Office Add-ins for testing in Windows](https://dev.office.com/docs/add-ins/testing/create-a-network-shared-folder-catalog-for-task-pane-and-content-add-ins)
    * [Sideload Office Add-ins for testing in Mac](https://dev.office.com/docs/add-ins/testing/sideload-an-office-add-in-on-ipad-and-mac)
* Change API endpoint URL
    * Refer to `getEnvironment` function in app/app.js
* Change add-in hosting URL, name, version etc
    * Refer to `SourceLocation` element of `manifest-writeup.xml`
* Set up additional build steps
    * Refer to `gulpfile.js`
* Check out module interfaces
    * Refer to exposed public methods at the end of every .js file and `callbacks` property within `self` declaration

### Key components of the add-in ###

* Markup
    * app/home/home.html
* Scripts
    * Libraries (scripts folder)
    * Application code (app folder)
* Styles (content folder)
* Configuration files
    * manifest writeup.xml
    * firebase.json

### Additional resources ###

* [Firebase deploying](https://firebase.google.com/docs/hosting/deploying)
* [Word JavaScript API reference](https://dev.office.com/reference/add-ins/word/word-add-ins-reference-overview)
* [Office UI Fabric](https://dev.office.com/fabric#/)
* [Office Add-in UX Design Patterns](https://github.com/OfficeDev/Office-Add-in-UX-Design-Patterns)
* [Interaction patterns for Office Add-ins](https://dev.office.com/docs/add-ins/design/ui-elements/interaction-patterns)
* [Create an Office Add-in using any editor](https://dev.office.com/docs/add-ins/get-started/create-an-office-add-in-using-any-editor)