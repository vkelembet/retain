function captureError(err) {
  if (typeof app !== 'undefined' && 
      typeof app.$header !== 'undefined' &&
      app.$header.length > 0 &&
      typeof app.showNotification !== 'undefined') {
    app.showNotification('Error', err.message);
  } else {
    var content = String(err);
    debug(content);
  }
}

// trim() polyfill for IE.
if(!String.prototype.trim) {  
  String.prototype.trim = function () {  
    return this.replace(/^\s+|\s+$/g,'');  
  };  
} 

// startsWith() polyfill for IE.
if (!String.prototype.startsWith) {
  Object.defineProperty(String.prototype, 'startsWith', {
    enumerable: false,
    configurable: false,
    writable: false,
    value: function(searchString, position) {
      position = position || 0;
      return this.lastIndexOf(searchString, position) === position;
    }
  });
}

// endsWith() polyfill for IE.
if (!String.prototype.endsWith) {
  Object.defineProperty(String.prototype, 'endsWith', {
    value: function(searchString, position) {
      var subjectString = this.toString();
      if (position === undefined || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
    }
  });
}

// Polyfilling window.onerror with try/catch.
// https://blog.getsentry.com/2016/01/04/client-javascript-reporting-window-onerror.html
function wrapErrors(fn) {
  // don't wrap function more than once
  if (!fn.__wrapped__) {
    fn.__wrapped__ = function () {
      try {
        return fn.apply(this, arguments);
      } catch (err) {
        captureError(err); // report the error
        throw err; // re-throw the error
      }
    };
  }

  return fn.__wrapped__;
}

function debug(content) {
  $('#debug').remove();
  $('body').append('<div id="debug"/>');
  $('#debug').html(content);
  $('#debug').click(function() {
    $('#debug').remove();
  });
}

var app = (function() {  // jshint ignore:line
  'use strict';

  var self = {
    mode: {
      debug: true,
      mocked: false
    },

    contentControl: {
      title: "Document Reference"
    }
  };

  function getEnvironment() {
    //rootURL: "https://retain.demo.relevant.software/v1",
    //rootURL: "https://retain.demo.relevant.software/",

    // rootURL: "https://test.pdf2html.demo.relevant.software/v1",
    // rootURL: "https://test.pdf2html.demo.relevant.software",

    return {
      rootURL: "https://retain.demo.relevant.software/v1",
      loginPath: "auth/login",
      logoutPath: "auth/logout",
      projectsPath: "project",
      articleContentPath: "document/html",
      notesPath: "note",
      tagsPath: "tags",

      admin: {
        rootURL: "https://retain.demo.relevant.software",
        projectPath: "projects/view",
        articlePath: "documents/view",
        notesHash: "#sectionD",
      }

    };
  }

  function getSelectors() {
    var s = {};

    s.notificationMessage = "#notification-message";
    s.activityIndicator   = "#activity-indicator";
    s.mainContent         = "#content-main";
    s.signInView          = "#view-sign-in";
    s.signInForm          = "#sign-in-form";
    s.signedUserView      = "#view-signed-user";
    s.userDetails         = "#user-details";
    s.projectsDropdown    = "#projects-dropdown";
    s.articleList         = "#article-list";
    s.tabs                = "#tabs";
    s.articleContent      = "#article-content";
    s.wordPad             = "#wordpad";
    s.autoContent         = "#auto-content";

    s.insertArticleTextButton       = "#insert-article-text-button";
    s.insertArticleTextAndRefButton = "#insert-article-text-and-ref-button";
    s.insertArticleRefButton        = "#insert-article-ref-button";
    
    s.insertWordPadNormalTextButton     = "#insert-wordpad-normal-button";
    s.insertWordPadTitleTextButton      = "#insert-wordpad-title-button";
    s.insertWordPadHeaderTextButton     = "#insert-wordpad-header-button";
    s.insertWordPadSubheaderTextButton  = "#insert-wordpad-subheader-button";

    return s;
  }

  function getTemplateSources() {
    var s = {};

    s.notificationMessage = $("#template-notification-message").html();
    s.mainContent         = $("#template-content-main").html();
    s.signInView          = $("#template-view-sign-in").html();
    s.signInForm          = $("#template-sign-in-form").html();
    s.signedUserView      = $("#template-view-signed-user").html();
    s.userDetails         = $("#template-user-details").html();
    s.projectsDropdown    = $("#template-projects-dropdown").html();
    s.articleList         = $("#template-article-list").html();
    s.tabs                = $("#template-tabs").html();
    
    s.articleListPlaceholder    = $("#template-article-list-placeholder").html();
    s.emptyArticleList          = $("#template-empty-article-list").html();
    s.articleContentPlaceholder = $("#template-article-content-placeholder").html();

    return s;
  }

  function getCompiledTemplates(sources) {
    var t = {}, s = sources;

    t.notificationMessage = Handlebars.compile(s.notificationMessage);
    t.mainContent         = Handlebars.compile(s.mainContent);
    t.signInView          = Handlebars.compile(s.signInView);
    t.signInForm          = Handlebars.compile(s.signInForm);
    t.signedUserView      = Handlebars.compile(s.signedUserView);
    t.userDetails         = Handlebars.compile(s.userDetails);
    t.projectsDropdown    = Handlebars.compile(s.projectsDropdown);
    t.articleList         = Handlebars.compile(s.articleList);
    t.tabs                = Handlebars.compile(s.tabs);

    t.articleListPlaceholder    = Handlebars.compile(s.articleListPlaceholder);
    t.emptyArticleList          = Handlebars.compile(s.emptyArticleList);
    t.articleContentPlaceholder = Handlebars.compile(s.articleContentPlaceholder);

    return t;
  }

  function initLayout(templates, selectors) {
    $(selectors.notificationMessage).html( templates.notificationMessage({ }) );
    $(selectors.mainContent).html( templates.mainContent({ }) );
    
    // Sign in UI.
    $(selectors.signInView).html( templates.signInView({ }) );
    $(selectors.signInForm).html( templates.signInForm({ }) );

    // Signed user UI.
    $(selectors.signedUserView).html( templates.signedUserView({ }) );
    $(selectors.userDetails).html( templates.userDetails({ }) );
    $(selectors.tabs).html( templates.tabs({ }) );
  }

  // Init UI objetcs when HTML already in place.
  function initjQueryObjects() {
    self.$close   = $('#notification-message-close');
    self.$header  = $('#notification-message-header');
    self.$body    = $('#notification-message-body');
    self.$log     = $('#log');
  }

  function showView(selector) {
    var $viewToShow = $(selector);

    $viewToShow.siblings().addClass("hidden");
    $viewToShow.removeClass("hidden").addClass("slide-left");
  }

  function showActivityIndicator() {
    $(self.selectors.activityIndicator).removeClass('hidden');
  }

  function hideActivityIndicator() {
    $(self.selectors.activityIndicator).addClass('hidden');
  }

  function notify(options) {
    var $close  = self.$close,
        $header = self.$header,
        $body   = self.$body;

    var o = options || {};

    o.header = o.header || '';
    o.html = o.html || '';
    o.text = o.text || '';

    $header.text(o.header);

    if (o.html !== '') {
      $body.html(o.html);
    } else {
      $body.text(o.text);
    }

    $(self.selectors.notificationMessage).slideDown('fast');
  }

  function showNotification(header, text) {
    header = header || '';
    text = text || '';

    self.$header.text(header);
    self.$body.text(text);

    $(self.selectors.notificationMessage).slideDown('fast');
  }

  function hideNotification() {
    self.$header.text('');
    self.$body.text('');

    $(self.selectors.notificationMessage).hide();
  }

  function initNotification() {
    self.$close.click(function() {
      $(self.selectors.notificationMessage).hide();
    });
  }

  function initLog() {
    var $log = self.$log;
    $log.click(function(e) {
      console.log('click');
      $log.addClass('hidden');
    });
  }

  function log(content) {
    if (!self.mode.debug) { return; }

    content = content || '';
    
    if (typeof content === 'object') {
      content = JSON.stringify(content);
    } else {
      content = String(content);
    }

    self.$log.append('<p>' + content + '</p></br>');
  }

  function showLog() {
    if (!self.mode.debug) { return; }
    self.$log.removeClass('hidden');
  }

  // Common initialization function (to be called from each page)
  self.initialize = function() {
    //self.showNotification('Debug', 'initialize');
    self.environment  = getEnvironment();
    self.sources      = getTemplateSources();
    self.templates    = getCompiledTemplates(self.sources);
    self.selectors    = getSelectors();
    
    initLayout(self.templates, self.selectors);
    initjQueryObjects();
    initNotification();
    initLog();
  };

  self.log = log;
  self.debug = debug;
  self.notify = notify;
  self.showLog = showLog;
  self.showView = showView;
  self.showNotification = showNotification;
  self.hideNotification = hideNotification;
  self.showActivityIndicator = showActivityIndicator;
  self.hideActivityIndicator = hideActivityIndicator;

  return self;
})();
