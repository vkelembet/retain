app.article = (function() {  // jshint ignore:line
    'use strict';

    var self = {
        $articleList:               undefined,
        $articleContent:            undefined,

        rendered:                   false,
        articles:                   [ ],
        indexedArticles:            { },
        selectedArticle:            undefined,
        autocompleteSets:           undefined,
        highlightCalloutVisible:    false,

        // TODO: remove "selected", extract to "note"
        selection: {
            selectedArticleText:        '',
            selectedPositionInArticle:  undefined,
            selectedLines:              [], // .t elements
            selectedTextNodes:          [],
            selectedDate:               undefined,
            selectedTag:                undefined,
            selectedRange:              undefined,
            
            note:                       null,
            paras:                      null
        },

        articlesContext: { 
            href: '#',
            articles: []
        },

        navigateToArticleWithId: null,

        positionToScroll: {
            pageNumber: 1,
            lineNumber: 1
        },

        callbacks: {
            onArticleListLoadStarted: function() {},
            onArticleListLoaded: function() {},
            onArticleListLoadFailure: function() {},
            
            onArticleSelected: function() {},
            onArticleDeselected: function() {},
            
            onArticleContentLoadStarted: function() {},
            onArticleContentLoaded: function(articleContent) {},
            onArticleContentLoadFailure: function() {},
            onArticleContentRendered: function() {},
            onArticelContentReady: function() {},

            onInsertArticleSelectedText: function() {},
            onInsertArticleSelectedTextAndRef: function() {},
            onInsertArticleRef: function() {},

            onArticleCreateNote: function() {},
            onArticleCreateHighlight: function() {}
        }
    };

    function isArticleContentRendered() {
        return self.rendered;
    }

    function setArticleContentRendered() {
        return self.rendered = true;
    }

    function initArticleContentHandlers() {
        var $container = $('#page-container');

        if ($container.length > 0) {
            $container.on('scroll', function() {
                if (self.highlightCalloutVisible) {
                    hideHighlightCallout();
                }
            });
        } else {
            console.log('Content is not rendered');
        }
    }

    function getArticleBy(id) {
        var articles = self.articlesContext.articles;
        // TODO: use articles indexed by id instead
        var result = $.grep(articles, function(e) { return e.id == id; });

        if (result.length == 0) {
            // not found
            return undefined;
        } else if (result.length == 1) {
            // access the property using result[0]
            return result[0];
        } else {
            // multiple items found
            return undefined;
        }
    }

    function initArticleListHandlers() {
        var $list = self.$articleList.find('.ms-List');
        
        $list.on('click', '.go-to-admin', function(e) {
            window.open($(this).data('href'), '_blank');
        });

        $list.on('click', '.insert-article-ref', function(e) {
            e.stopImmediatePropagation();

            var articleId = $(this).data('id'),
                article = getArticleBy(articleId),
                ref = buildArticleRef(article);

            self.callbacks.onInsertArticleRef(ref);
        });

        $list.on('click', 'li', onArticleListItemClick);
    }

    function fitArticleContent() {
        if (typeof pdf2htmlEX.defaultViewer !== 'undefined') {
            setTimeout(function(){
                //pdf2htmlEX.defaultViewer.rescale(0);
                pdf2htmlEX.defaultViewer.fit_width();
            }, 0);
        }
    }

    function renderArticleContent(articleContent) {
        var selector    = app.selectors.articleContent,
            $container  = $(selector),
            container   = $container.get(0);

        console.time('parseHTML');
        var html = $.parseHTML(articleContent);
        console.timeEnd('parseHTML');

        $container.empty();

        console.time('append');
        $container.append(html);
        console.timeEnd('append');

        // TODO: try to insert after last style but before inserting content
        $container.append('<style>@media screen{.pc{display:none;}}</style>');

        self.callbacks.onArticleContentRendered();
    }

    function clearArticleList() {
        self.articles                   = [];
        self.indexedArticles            = {};
        self.articlesContext.articles   = [];
        self.selectedArticle            = undefined;
        pdf2htmlEX.defaultViewer        = undefined;
        clearSelectedArticleText();

        var template = app.templates.articleListPlaceholder({ });
        self.$articleList.html( template );
    }

    function destroyHighligherCallout() {
        var $firstWrapper = self.selection.$firstWrapper;
        if (typeof $firstWrapper !== 'undefined') {
            //$firstWrapper.hideHighlightCallout();
            hideHighlightCallout();
            $firstWrapper.highlighter('destroy');
        }
    }

    function clearArticleContent() {
        hideHighlightCallout();

        pdf2htmlEX.defaultViewer = undefined;
        clearSelectedArticleText();

        if (self.$articleContent.length > 0) {
            var template = app.templates.articleContentPlaceholder({ });
            self.$articleContent.html( template );
        }

        self.rendered = false;
    }

    function renderArticleList() {
        var ctx = self.articlesContext,
            template = app.templates.articleList(ctx);

        self.$articleList.html(template);   
    }

    function renderEmptyArticleList() {
        var template = app.templates.emptyArticleList({ });
        self.$articleList.html( template );
    }

    function resetScrollPosition() {
        setScrollPosition(1, 1);
    }

    function setScrollPosition(pageNumber, lineNumber) {
        self.positionToScroll.pageNumber = pageNumber;
        self.positionToScroll.lineNumber = lineNumber;
    }

    function scrollToPosition() {
        var page = self.positionToScroll.pageNumber,
            line = self.positionToScroll.lineNumber;

        scrollToPage(page);
        //scrollToLine(line);
    }

    function scrollToPage(page) {
        if (typeof pdf2htmlEX.defaultViewer !== 'undefined') {
            setTimeout(function() {
                pdf2htmlEX.defaultViewer.scroll_to(page - 1);
            }, 0);
        }
    }

    function scrollToLine(line) {
        var $container = $('#page-container');
        
        var $page = $container.find('#pf' + page),
            pageTop = $page.position().top;
        
        var $line = $page.find('.t').eq(line - 1),
            lineTop = $line.position().top;

        var pos = pageTop + lineTop;
        
        $container.scrollTop(pos);
    }

    function getArticleContentSuccess(articleContent) {
        self.callbacks.onArticleContentLoaded(articleContent);
    }

    function getArticleContentFailure(data) {
        self.callbacks.onArticleContentLoadFailure(data);
    }

    function checkArticleContentResponse(data, success, failure) {
        if (typeof data !== 'undefined') {
            var articleContent = data.data;

            if (data.success == true && typeof articleContent !== 'undefined') {
                success(articleContent);
                return;
            }
        }

        failure(data);
    }

    function getArticleContent(articleUUID) {
        self.callbacks.onArticleContentLoadStarted();

        var success = getArticleContentSuccess,
            failure = getArticleContentFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.articleContentPath + '/?uuid=' + articleUUID;

        var authorization = 'Bearer ' + app.user.token;

        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkArticleContentResponse(data, success, failure);
        });

        getting.fail(function(data) {
            failure(data);
        });
    }

    function getArticleIndex() {
        return self.indexedArticles;
    }

    function createArticleIndex(articles) {
        var len = articles.length, 
            i, 
            article, 
            articleId,
            indexedArticles = {};

        for (i = 0; i < len; ++i) {
            article = articles[i];
            articleId = article.id;

            indexedArticles[articleId] = article;
        }

        return indexedArticles;
    }

    function getArticlesSuccess(articles) {
        self.articles                   = articles;
        self.indexedArticles            = createArticleIndex(articles);
        self.articlesContext.articles   = articles;
                
        if (articles.length > 0) {
            renderArticleList();
            initArticleListHandlers();
        } else {
            renderEmptyArticleList();
        }

        self.callbacks.onArticleListLoaded(articles);
    }

    function getArticlesFailure(data) {
        self.callbacks.onArticleListLoadFailure(data);
    }

    function checkGetArticlesResponse(data, success, failure) {
        if (typeof data !== 'undefined' && data.success == true) {
            var project = data.data;

            if (typeof project !== 'undefined') {
                var articles = project.documents;

                if (typeof articles !== 'undefined') {
                    success(articles);
                    return;
                }
            } 
        }

        failure(data);
    }

    function onArticleSelected($listItem, className) {
        // Deselect sibling list items.
        $listItem.siblings('.ms-ListItem').removeClass(className);

        // Select current list item.
        $listItem.addClass(className);            

        var articleId = $listItem.data('id');
        self.selectedArticle = getArticleBy(articleId);

        pdf2htmlEX.defaultViewer = undefined;
        clearSelectedArticleText();

        self.callbacks.onArticleSelected(self.selectedArticle);               
    }

    function onArticleDeselected($listItem, className) {
        // Deselect current article.
        $listItem.removeClass(className);

        self.selectedArticle = undefined;
        clearArticleContent();

        self.callbacks.onArticleDeselected();
    }

    function onArticleListItemClick(e) {
        var $listItem = $(this),
            className = 'is-selected';

        if ($listItem.hasClass(className)) {
            onArticleDeselected($listItem, className);
        } else {
            onArticleSelected($listItem, className);
        }
    }

    function selectArticleByNavigation(articleId) {
        pdf2htmlEX.defaultViewer    = undefined;
        self.selectedArticle        = undefined;
        
        clearSelectedArticleText();

        self.navigateToArticleWithId = articleId;

        selectArticleListItemIfNotSelected(articleId);
    }

    function selectArticleListItemIfNotSelected(articleId) {
        var selector = '.ms-List li[data-id=' + articleId + ']',
            $listItem = self.$articleList.find(selector);

        if ($listItem.hasClass('is-selected')) {
            // Referenced article already selected.
            console.log('Referenced article already selected.');
            // TODO: Check if article content tab is opened
        } else {
            $listItem.click();
        }
    }

    function getArticles(projectId) {
        self.callbacks.onArticleListLoadStarted();

        var env = app.environment,
            url = env.rootURL + '/' + env.projectsPath + '/' + projectId + '?expand=documents';

        var authorization = 'Bearer ' + app.user.token;

        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType   : 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkGetArticlesResponse(data, getArticlesSuccess, getArticlesFailure);
        });

        getting.fail(function(data) {
            getArticlesFailure(data);
        });

        // debug fake articles (documents)
        // success([
        //     {
        //         "id": 110,
        //         "project_id": 40,
        //         "user": 11,
        //         "title": "SAMPLE - CRM4 Performance and Scalability Assessment of Customer Implementation.docx",
        //         "uploaded_date": 1466149051,
        //         "user_ip": "194.44.216.70",
        //         "user_agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
        //         "html_file": "110.html",
        //         "uuid": "5a241aea-345e-11e6-967c-061c72b17085"
        //     },
        //     {
        //         "id": 111,
        //         "project_id": 40,
        //         "user": 11,
        //         "title": "APL-sample.docx",
        //         "uploaded_date": 1466155426,
        //         "user_ip": "194.44.216.70",
        //         "user_agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/601.6.17 (KHTML, like Gecko) Version/9.1.1 Safari/601.6.17",
        //         "html_file": "111.html",
        //         "uuid": "329957a0-346d-11e6-967c-061c72b17085"
        //     }
        // ]);
    }

    function renderNoteSelections(articleNotes) {
        var $allLines = self.$articleContent.find('.t.highlighted');
        unhighlightSelectedLines($allLines);

        var notes = articleNotes;

        if (typeof notes !== 'undefined' && notes && notes.constructor === Array) {
            var i, note, len = notes.length;
            for (i = 0; i < len; ++i) {
                note = notes[i];
                renderNoteSelection(note);
            }
        }
    }

    function renderNoteSelection(note) {
        if (typeof note !== 'undefined' && typeof note.positions !== 'undefined') {
            var positions = note.positions;
            if (positions != '') {
                try {
                    var coords = JSON.parse(positions);
                    if ( isSelectionCoordinatesValid(coords) ) {
                        // Find page.
                        // TODO: across pages.
                        var pageSelector = '.pf[data-page-no="' + coords.startPage + '"]';
                        var $startPage = self.$articleContent.find(pageSelector);
                        
                        var startLine = coords.startLine - 1;
                        var endLine = coords.endLine;
                        
                        var $allLines = $startPage.find('.t');

                        var $lines = $allLines.slice(startLine, endLine);

                        highlightSelectedLines($lines);
                    }
                } catch (e){
                    console.log(e);
                }
            }
        }
    }

    //function get

    // coords: { startPage: 1, endPage: 1, startLine: 20, endLine: 20}
    function isSelectionCoordinatesValid(coords) {
        if ( $.isNumeric(coords.startPage) &&
             $.isNumeric(coords.endPage) &&
             $.isNumeric(coords.startLine) &&
             $.isNumeric(coords.endLine) ) {
                 return true;
       }
       return false;
    }

    function nextNode(node) {
        if (node.hasChildNodes()) {
            return node.firstChild;
        } else {
            while (node && !node.nextSibling) {
                node = node.parentNode;
            }
            if (!node) {
                return null;
            }
            return node.nextSibling;
        }
    }

    function getRangeSelectedNodes(range) {
        var node    = range.startContainer;
        var endNode = range.endContainer;

        // Special case for a range that is contained within a single node
        console.log('node == endNode:', node == endNode);
        if (node == endNode) {
            return [node];
        }

        // Iterate nodes until we hit the end container
        var rangeNodes = [];
        while (node && node != endNode) {
            node = nextNode(node);
            console.log('next node:', node);
            rangeNodes.push(node);
        }

        // Add partially selected nodes at the start of the range
        node = range.startContainer;
        while (node && node != range.commonAncestorContainer) {
            rangeNodes.unshift(node);
            node = node.parentNode;
        }

        return cleanArray(rangeNodes);
    }

    // Will remove all falsy values: undefined, null, 0, false, NaN and ""
    function cleanArray(actual) {
        var newArray = new Array();
        for (var i = 0; i < actual.length; i++) {
            if (actual[i]) {
                newArray.push(actual[i]);
            }
        }
        return newArray;
    }

    function getSelectionRange() {
        if (window.getSelection) {
            var sel = window.getSelection();
            if (!sel.isCollapsed) {
                return sel.getRangeAt(0);
            }
        }
        return null
    }

    // Split text note if needed in order to match selection range.
    function getRangeTextNodeFromNode(textNode, range) {
        if (textNode === range.startContainer && textNode === range.endContainer) {
            // Split into 3 nodes and return one in the middle.
            var textNodeMiddle = textNode.splitText(range.startOffset);
            var textNodeRight = textNodeMiddle.splitText(range.endOffset);
            return textNodeMiddle;
        } else if (textNode === range.startContainer) {
            // Split into 2 nodes and return the last one.
            var textNodeRight = textNode.splitText(range.startOffset);
            return textNodeRight;
        } else if (textNode === range.endContainer) {
            // Split into 2 nodes and return the first one.
            var textNodeRight = textNode.splitText(range.endOffset);
            var textNodeLeft = textNode;
            return textNodeLeft;
        }

        return textNode;
    }

    function getSelectedTextFromNode(node, range) {
        var value = node.nodeValue;

        if (node === range.startContainer && node === range.endContainer) {
            return value.substring(range.startOffset, range.endOffset);
        } else if (node === range.startContainer) {
            return value.substring(range.startOffset);
        } else if (node === range.endContainer) {
            return value.substring(0, range.endOffset);
        }

        return value;
    }

    function isText(node) {
        return (node.nodeType === 3);
    }

    function isElement(node) {
        return (node.nodeType === 1);
    }

    // See http://jsfiddle.net/WeWy7/3/
    // Add space between selected text nodes if they are in different ".t" containers
    function getSelectedArticleTextData(range) {
        var selectedText = '', 
            position = undefined,
            rangeTextNodes = [],
            lineNodes = [],
            rangeTextNode = undefined;

        if (range !== null) {
            var selectedNodes       = getRangeSelectedNodes(range),
                clonedSelectedNodes = range.cloneContents(),
                value, nextValue, node, i, len, lineNode;

            if (selectedNodes.length > 0) {
                // Get line number and page number.
                position = getPositionInArticleForRange(selectedNodes[0]);
            }

            // Get all text nodes
            for (i = 0, len = selectedNodes.length; i < len; i++) {
                node = selectedNodes[i];
                value = node.nodeValue;

                if (isText(node) && value !== null && value !== '') {
                    rangeTextNode = getRangeTextNodeFromNode(node, range);
                    rangeTextNodes.push(rangeTextNode);
                    lineNode = $(rangeTextNode.parentNode).closest('.t').get(0);
                } else if (isElement(node)) {
                    lineNode = $(node).closest('.t').get(0);
                }

                lineNodes.push(lineNode);
            }

            // Check pairs - if not same parent, insert space between
            var nextNode, nextIndex, 
                shouldInsertSpace = false, 
                nodeAncestor, nextAncestor;

            for (i = 0, len = rangeTextNodes.length; i < len; i++) {
                node        = rangeTextNodes[i];
                value       = node.nodeValue;
                nextIndex   = i + 1;
                
                if (value !== null) {
                    value = getSelectedTextFromNode(node, range);

                    selectedText = selectedText + value;

                    if (nextIndex < len) {
                        nextNode = rangeTextNodes[nextIndex];
                        nextValue = nextNode.nodeValue;

                        nodeAncestor = $(node).closest('.t').get(0);
                        nextAncestor = $(nextNode).closest('.t').get(0);

                        shouldInsertSpace = (nodeAncestor !== nextAncestor) &&
                            !selectedText.endsWith(' ') && !nextValue.startsWith(' ');

                        if (shouldInsertSpace) {
                            selectedText = selectedText + ' ';
                        }
                    }
                }
            }
        }

        return {
            range: range,
            text: selectedText,
            position: position,
            selectedNodes: selectedNodes,
            clonedSelectedNodes: clonedSelectedNodes,
            rangeTextNodes: rangeTextNodes,
            lineNodes: lineNodes
        };
    }

    function deselectAll() {
        if (window.getSelection) {
            if (window.getSelection().empty) {  // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) {  // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) {  // IE?
            document.selection.empty();
        }
    }

    function getPositionInArticleForRange(firstSelectedNode) {

        var lineSelector = '.t',
            pageSelector = '.pf',
            $line = $(firstSelectedNode).closest(lineSelector),
            $page = $(firstSelectedNode).closest(pageSelector);

        var position = {
            pageNumber: $page.prevAll(pageSelector).length + 1,
            lineNumber: $line.prevAll(lineSelector).length + 1
        };

        return position;
    }

    function unwrapSelection() {
        var wrappers = self.selection.wrappers;

        if (typeof wrappers !== 'undefined') {
            // Replace spans with text notes.
            var i, span, len = wrappers.length;

            for (i = 0; i < len; ++i) {
                span = wrappers[i];
                $(span).contents().unwrap();
            }
        }
    }

    function clearSelectedArticleText() {
        var s = self.selection;

        s.selectedArticleText          = '';
        s.selectedPositionInArticle    = undefined; // TODO: remove (?)
        s.selectedTextPositions        = undefined;
        s.selectedLines                = undefined;
        s.selectedNodes                = undefined;
        s.clonedSelectedNodes          = undefined;
        s.selectedTextNodes            = undefined;
        s.selectedTag                  = undefined;
        s.selectedDate                 = undefined;
    }

    function buildArticleRef(article, position, paragraphs) {
        position = position || {
            pageNumber: 1,
            lineNumber: 1
        };

        return {
            title:      article.title,
            userId:     article.user,
            projectId:  article.project_id,
            articleId:  article.id,
            pageNumber: position.pageNumber,
            lineNumber: position.lineNumber,
            paragraphs: paragraphs
        }
    }

    function isArticleContentSelectedOnly(range) {
        if (range == null) {
            return false;
        } 

        var contained = range.commonAncestorContainer,
            container = self.$articleContent.get(0);
        
        if (!$.contains(container, contained)) {
            return false;
        }

        return true;
    }

    function onArticleTextSelected(e) {
        var range = getSelectionRange();

        //deemphasizeSelectedLines();
        clearSelection();

        if (!isArticleContentSelectedOnly(range)) {
            return; 
        }
        
        var data = getSelectedArticleTextData(range);

        console.log('---------------------------------');
        console.log('selected text:', data.text);
        console.log('selected nodes:', data.selectedNodes);
        console.log('range text nodes:', data.rangeTextNodes);
        console.log('line nodes:', data.lineNodes);
        console.log('range:', data.range);
        console.log('---------------------------------');

        // Cache new selection and show highlight callout.
        if (isArticleTextSelected(data.text)) {
            preserveSelection(data);

            // Wrap text nodes and preserve first wrapper.
            var wrappers = wrapSelectedTextNodes(data.rangeTextNodes);
            self.selection.wrappers = wrappers;

            var $firstWrapper = $(wrappers[0]);
            self.selection.$firstWrapper = $firstWrapper;

            showHighlightCallout($firstWrapper);
            
            //var $selectedLines = $(data.lineNodes);
            //emphasizeSelectedLines($selectedLines);
            
            self.selection.selectedTextPositions = getSelectedArticleTextPosition();
        }
    }

    function clearSelection() {
        hideHighlightCallout();
        unwrapSelection();
        clearSelectedArticleText();
        deselectAll();
    }

    function wrapSelectedTextNodes(rangeTextNodes) {
        var wrappers = [], span, i, node, text;
 
        for (i = 0; i < rangeTextNodes.length; ++i) {
            node = rangeTextNodes[i];
            console.log('node:', node);

            if (typeof node !== 'undefined') {
                text = node.textContent;

                span = document.createElement("span");
                span.classList.add('selected');
                span.textContent = text;
                
                // Note: parentElement is null in IE
                // https://connect.microsoft.com/IE/feedback/details/786279/node-parentelement-incorrectly-returns-null
                node.parentNode.insertBefore(span, node);
                
                // Note:
                // remove() is not supported by IE
                // removeNode() is not supported ny Webkit
                // https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/remove#Browser_compatibility
                $(node).remove();

                wrappers.push(span);
            }
        }

        return wrappers;
    }

    // data - selected article text data
    function preserveSelection(data) {
        self.selection.selectedArticleText        = data.text;
        self.selection.selectedPositionInArticle  = data.position;
        self.selection.selectedLines              = data.lineNodes;
        self.selection.selectedNodes              = data.selectedNodes;
        self.selection.clonedSelectedNodes        = data.clonedSelectedNodes;
        self.selection.selectedTextNodes          = data.rangeTextNodes;
        self.selection.selectedRange              = data.range;
    }

    function emphasizeSelectedLines($selectedLines) {
        $selectedLines.addClass('selected');
    }

    function deemphasizeSelectedLines() {
        var $selectedLines = $(self.selection.selectedLines);
        $selectedLines.removeClass('selected');
    }

    function highlightSelectedLines($selectedLines) {
        $selectedLines.addClass('highlighted');
    }

    function unhighlightSelectedLines($selectedLines) {
        $selectedLines.removeClass('highlighted');
    }

    function isArticleTextSelected(selectedText) {
        return (typeof selectedText !== 'undefined' && selectedText != '');
    }

    function showHighlightCallout($selectedTextWrapper) {
        var $el = $selectedTextWrapper;

        if (typeof $el !== 'undefined' && $el.length > 0) {
            initHighlightCallout($el);
            initHighlightCalloutEventHandlers($el);

            // Show popover.
            $el.highlighter('show');
            self.highlightCalloutVisible = true;
        }
    }

    function initHighlightCallout($el) {
        $el.highlighter({
            popover: {
                placement: 'auto',
                viewport: { 
                    selector: '#view-signed-user', 
                    padding: 10
                },
                content: function() {
                    return $('#template-popover-content').html();
                }
            },

            autocompleteSets: self.autocompleteSets
        });
    }

    function setHighlighterState(state) {
        var $el = self.selection.$firstWrapper;
        if (typeof $el !== 'undefined' && $el.length > 0) {
            $el.highlighter('state', { state: state });
        }
    }

    function initHighlightCalloutEventHandlers($el) {
        $el.on('selected.highlighter.date', function(e) {
            self.selection.selectedDate = e.date;
        });

        $el.on('deselected.highlighter.date', function(e) {
            self.selection.selectedDate = undefined;
        });

        $el.on('shown.highlighter.pick-tag.page', function(e) {
            app.tag.getTags();
        });

        $el.on('shown.highlighter.add-note.page', function(e) {
            $el.highlighter('renderAddNotePage', {
                selectedTag:    self.selection.selectedTag,
                selectedDate:   self.selection.selectedDate
            });
        });

        $el.on('selected.highlighter.tag', function(e) {
            self.selection.selectedTag = e.tag;
        });

        $el.on('deselected.highlighter.tag', function(e) {
            self.selection.selectedTag = undefined;
        });

        $el.on('clicked.highlighter.add-tag.btn', function(e) {
            app.tag.createTag(e.tagTitle);
        });

        $el.on('clicked.highlighter.add-note.btn', function(e) {
           self.selection.note   = e.note;
           self.selection.paras  = e.paras;
           
           console.log('clicked add-note.btn');
           self.callbacks.onArticleCreateNote();
        });

        $el.on('clicked.highlighter.add-highlight.btn', function(e) {
           console.log('clicked add-highlight.btn');
           self.callbacks.onArticleCreateHighlight();
        });

        $el.on('clicked.highlighter.insert.btn', function(e) {
            var action  = e.action, 
                paras   = e.paragraphs || '';

            var text    = self.selection.selectedArticleText,
                article = self.selectedArticle,
                pos     = self.selection.selectedPositionInArticle,
                ref     = buildArticleRef(article, pos, paras);

            switch (e.action) {
                case 'insert-article-text':
                    self.callbacks.onInsertArticleSelectedText(text);
                    break;

                case 'insert-article-text-and-ref':
                    self.callbacks.onInsertArticleSelectedTextAndRef(text, ref);
                    break;

                case 'insert-article-ref':
                    self.callbacks.onInsertArticleRef(ref);
                    break;
            }
        });
    }

    // tags: [ { "id": 7, "title": "tag" } ]
    // selectedTag: { "id": 7, "title": "tag" }
    function renderLoadedTags(tags) { 
        var selectedTag     = self.selection.selectedTag;    
        var $firstWrapper   = self.selection.$firstWrapper;
        
        if (typeof $firstWrapper !== 'undefined' && $firstWrapper.length > 0) {
            $firstWrapper.highlighter('renderPickTagPage', {
                tags: tags, 
                selectedTag: selectedTag
            });
        }
    }

    function hideHighlightCallout() {
        var $el = self.selection.$firstWrapper;
        hideHighlightCalloutForElement($el);
    }

    function hideHighlightCalloutForElement($el) {
        if (typeof $el !== 'undefined' && $el.length > 0) {
            $el.highlighter('hide');
            self.highlightCalloutVisible = false;
        }
    }

    function destroyHighlightCallout($el) {
         if (typeof $el !== 'undefined' && $el.length > 0) {
            $el.highlighter('destroy');
        }       
    }

    function getSelectedArticleTextPosition() {
        if (typeof self.selection !== 'undefined') {
            // Coordinates of selected article text.
            var positions = {
                startPage: undefined,
                endPage: undefined,
                startLine: undefined,
                endLine: undefined
            }

            var $lines = $(self.selection.selectedLines),
                $firstLine = $lines.first(),
                $lastLine = $lines.last();

            positions.startLine = $firstLine.prevAll('.t').length + 1;
            positions.endLine   = $lastLine.prevAll('.t').length + 1;
            positions.startPage = $firstLine.closest('.pf').data('page-no');
            positions.endPage   = $lastLine.closest('.pf').data('page-no');

            return positions;
        }

        return undefined;
    }

    function initArticleContent() {
        self.$articleContent.on('mouseup', onArticleTextSelected);
    }

    function deinitArticleContent() {
        self.$articleContent.off('mouseup');
    }

    function setHighlightAutocompleteSets(sets) {
        self.autocompleteSets = sets;
    }

    self.deinit = function() {
        console.log('article deinit');
        
        hideHighlightCallout();
        //destroyHighlightCallout($el);

        deinitArticleContent();

        self.$articleList       = undefined;
        self.$articleContent    = undefined;
    }

    self.initialize = function() {    
        var env = app.environment.admin;
        
        self.rendered = false;
        self.articlesContext.href = env.rootURL + '/' + env.articlePath + '/';

        self.$articleList       = $(app.selectors.articleList);
        self.$articleContent    = $(app.selectors.articleContent);

        initArticleContent();
        clearArticleList();
        clearArticleContent();
    }

    self.getArticles = getArticles;
    self.getArticleBy = getArticleBy;
    self.clearSelection = clearSelection;
    self.buildArticleRef = buildArticleRef;
    self.getArticleIndex = getArticleIndex;
    self.scrollToPosition = scrollToPosition;
    self.clearArticleList = clearArticleList;
    self.renderLoadedTags = renderLoadedTags;
    self.fitArticleContent = fitArticleContent;
    self.setScrollPosition = setScrollPosition;
    self.getArticleContent = getArticleContent;
    self.resetScrollPosition = resetScrollPosition;
    self.clearArticleContent = clearArticleContent;
    self.setHighlighterState = setHighlighterState;
    self.renderArticleContent = renderArticleContent;
    self.renderNoteSelections = renderNoteSelections;
    self.renderEmptyArticleList = renderEmptyArticleList;
    self.isArticleContentRendered = isArticleContentRendered;
    self.setArticleContentRendered = setArticleContentRendered;
    self.selectArticleByNavigation = selectArticleByNavigation;
    self.initArticleContentHandlers = initArticleContentHandlers;
    self.setHighlightAutocompleteSets = setHighlightAutocompleteSets;
    self.getSelectedArticleTextPosition = getSelectedArticleTextPosition;

    return self;
})();

