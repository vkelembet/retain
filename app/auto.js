app.auto = (function() {  // jshint ignore:line
    'use strict';

    var self = {
        $autoContent: undefined
    };

    function initAutoHandlers() {
        if ($.fn.Dialog) {
            $('.ms-Dialog').Dialog();
        }
    }

    self.initialize = function() {
        self.$autoContent = $(app.selectors.autoContent);

        initAutoHandlers();
    };

    return self;
})();