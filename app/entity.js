app.entity = (function() {  // jshint ignore:line
    'use strict';

    var self = {
        $wordPad: undefined,

        $insertNoteTextButton: undefined,
        $insertNoteTextWithRefButton: undefined,
        $insertNoteRefButton: undefined,

        $insertWordPadNormalTextButton: undefined,
        $insertWordPadTitleTextButton: undefined,
        $insertWordPadHeaderTextButton: undefined,
        $insertWordPadSubheaderTextButton: undefined,

        entities: undefined,
        indexedEntities: undefined,
        autocompleteSets: undefined,

        callbacks: {
            onEntitiesLoadStarted: function() {},
            onEntitiesLoaded: function() {},
            onEntitiesLoadFailure: function() {},

            onSendStyledTextToWordDocument: function() {}
        }
    };

    function isEntitiesLoaded() {
        var entities = self.entities;
        if (typeof entities !== 'undefined' && entities.constructor === Array) {
            return true;
        }
        return false;       
    }

    function getAutocompleteSets() {
        return self.autocompleteSets;
    }

    function getSelectedWordPadText() {
        var selectedText = self.$wordPad.getSelection().text;

        if (typeof selectedText === 'undefined' || selectedText === '') {
            selectedText = self.$wordPad.val();
            self.$wordPad.val('');
        }

        return selectedText;
    }

    function initWordPadInsertionButtons() {
        var s = app.selectors;

        self.$insertWordPadNormalTextButton     = $(s.insertWordPadNormalTextButton);
        self.$insertWordPadTitleTextButton      = $(s.insertWordPadTitleTextButton);
        self.$insertWordPadHeaderTextButton     = $(s.insertWordPadHeaderTextButton);
        self.$insertWordPadSubheaderTextButton  = $(s.insertWordPadSubheaderTextButton);

        self.$insertWordPadNormalTextButton.on('click', wrapErrors(function(e) {
            sendSelectedText('Normal');
        }));

        self.$insertWordPadTitleTextButton.on('click', wrapErrors(function(e) {
            sendSelectedText('Title');
        }));

        self.$insertWordPadHeaderTextButton.on('click', wrapErrors(function(e) {
            sendSelectedText('Heading 1');
        }));

        self.$insertWordPadSubheaderTextButton.on('click', wrapErrors(function(e) {
            sendSelectedText('Heading 2');
        }));
    }

    function deinitWordPadInsertionButtons() {
        console.log("$insertWordPadNormalTextButton", self.$insertWordPadNormalTextButton);
        self.$insertWordPadNormalTextButton.off('click');
        self.$insertWordPadTitleTextButton.off('click');
        self.$insertWordPadHeaderTextButton.off('click');
        self.$insertWordPadSubheaderTextButton.off('click');

        self.$insertWordPadNormalTextButton     = undefined;
        self.$insertWordPadTitleTextButton      = undefined;
        self.$insertWordPadHeaderTextButton     = undefined;
        self.$insertWordPadSubheaderTextButton  = undefined;
    }

    function sendSelectedText(style) {
        var selectedText = getSelectedWordPadText();
        if (selectedText !== '') {
            self.callbacks.onSendStyledTextToWordDocument(selectedText, style);
        }
    }

    function getIndexedEntities(entities) {
        // Build a map of entities by article id, type etc.
        var len = entities.length, 
            articles = { 
                autoEntities: {
                    all: [],
                    d: [], /* date */
                    m: [], /* quantities containing £,$,€ */
                    c: [], /* company */
                    p: [], /* person */
                    q: [], /* quantity */
                    g: [], /* company, person, organization */
                    l: []  /* city, country, continent, state or county */
                },
                tags: [],
                dates: []
            },
            entity, article, articleId;

        // Debug.
        var types = { }, count;

        for (var i = 0; i < len; i++) {
            entity      = entities[i];
            articleId   = entity.doc_id;

            // DEBUG
            if (typeof types[entity.type] == 'undefined') {
                types[entity.type] = 0;
            } else {
                count = types[entity.type];
                count++;
                types[entity.type] = count;
            }

            // if (typeof articleId == 'undefined') {
            //     //throw new Error('Entity should contain article ID field ("doc_id")');
            //     console.log("Entity should contain article ID field (doc_id)");
            //     break;
            // }

            // if (typeof entity.name == 'undefined') {
            //     throw new Error('Entity should contain value field ("name")');
            // }

            if (typeof articles[articleId] == 'undefined') {
                articles[articleId] = {
                    autoEntities: {
                        all: [],
                        d: [], /* date */
                        m: [], /* quantities containing £,$,€ */
                        c: [], /* company */
                        p: [], /* person */
                        q: [], /* quantity */
                        g: [], /* company, person, organization */
                        l: []  /* city, country, continent, state or county */
                    },
                    tags: []
                };
            }

            var value   = entity.name, 
                type    = entity.type;

            if (type == 'tag') {
                // Tags are not taking part in autocomplete
                articles.tags.push(entity)
                articles[articleId].tags.push(entity);
            } else {
                
                switch (type) {
                    case 'date':
                        articles.autoEntities.d.push(value);
                        articles[articleId].autoEntities.d.push(value);
                        break;
                    case 'Person':
                        articles.autoEntities.p.push(value);
                        articles[articleId].autoEntities.p.push(value);

                        articles.autoEntities.g.push(value);
                        articles[articleId].autoEntities.g.push(value);
                        break;
                    case 'Company':
                        articles.autoEntities.c.push(value);
                        articles[articleId].autoEntities.c.push(value);

                        articles.autoEntities.g.push(value);
                        articles[articleId].autoEntities.g.push(value);
                        break;
                    case 'Quantity':
                        articles.autoEntities.q.push(value);
                        articles[articleId].autoEntities.q.push(value);

                        if (isMoneyQuantity(value)) {
                            articles.autoEntities.m.push(value);
                            articles[articleId].autoEntities.m.push(value);
                        }

                        break;
                    case 'Organization':
                        articles.autoEntities.g.push(value);
                        articles[articleId].autoEntities.g.push(value);
                        break;
                    case 'City':
                    case 'Country':
                    case 'Continent':
                    case 'StateOrCounty':
                        // TODO: 'Region', 'GeographicFeature'?
                        articles.autoEntities.l.push(value);
                        articles[articleId].autoEntities.l.push(value);
                        break;
                }

                articles.autoEntities.all.push(value);
                articles[articleId].autoEntities.all.push(value);
            }
        }

        // Debug
        console.log('types:', types);

        return articles;
    }

    function isMoneyQuantity(value) {
        var result = value.indexOf('£') !== -1 ||
            value.indexOf('$') !== -1 || 
            value.indexOf('€') !== -1;

        return result;
    }

    function buildAutocompleteSets(getAutoEntitiesFn) {
        return [
            { 
                words: [],
                match: /([A-Za-z0-9_\$£€>]{2,})$/,
                search: function (term, callback) {
                    var auto = getAutoEntitiesFn(), 
                        words;

                    if (term.startsWith('>g')) {
                        term = term.substr(2);
                        words = auto.g;
                    } else if (term.startsWith('>c')) {
                        term = term.substr(2);
                        words = auto.c;
                    } else if (term.startsWith('>p')) {
                        term = term.substr(2);
                        words = auto.p;
                    } else if (term.startsWith('>q')) {
                        term = term.substr(2);
                        words = auto.q;
                    } else if (term.startsWith('>l')) {
                        term = term.substr(2);
                        words = auto.l;
                    } else if (
                        term.startsWith('>$') || 
                        term.startsWith('>£') ||
                        term.startsWith('>€')
                        ) {
                            term = term.substr(2);
                            words = auto.m;
                    } else {
                        words = auto.all;
                    }

                    term = term.toLowerCase();

                    callback($.map(words, function (word) {
                        var normalizedWord = word.toLowerCase();
                        return normalizedWord.indexOf(term) === 0 ? word : null;
                    }));
                },
                index: 1,
                replace: function (word) {
                    return word + ' ';
                }
            }
        ];
    }

    function sortAutoEntities(entities) {
        entities.all.sort();
        entities.g.sort();
        entities.c.sort();
        entities.p.sort();
        entities.q.sort();
        entities.l.sort();
    }

    function clearAutocomplete() {
        self.$wordPad.textcomplete('destroy');
    }

    function initAutocomplete(autoEntities) {
        sortAutoEntities(autoEntities);
        
        self.autocompleteSets = buildAutocompleteSets(function() {
            return autoEntities;
        });

        // TODO: extract to wordpad component;
        self.$wordPad.textcomplete(self.autocompleteSets);
    }

    function initAutocompleteForProject() {
        clearAutocomplete();
        var projectAutoEntities = self.indexedEntities.autoEntities;
        initAutocomplete(projectAutoEntities);       
    }

    // TODO: fix for case when self.indexedEntities[articleId] undefined
    function initAutocompleteForArticle(articleId) {
        clearAutocomplete();
        var indexedEntities = self.indexedEntities[articleId];
        if (typeof indexedEntities !== 'undefined') {
            var articleAutoEntities = indexedEntities.autoEntities;
            initAutocomplete(articleAutoEntities);
        }
    }

    function getEntitiesSuccess(entities) {
        self.entities           = entities;
        self.indexedEntities    = getIndexedEntities(entities);

        self.callbacks.onEntitiesLoaded(entities);
    }

    function getEntitiesFailure(data) {
        self.entities           = undefined;
        self.indexedEntities    = undefined;
        
        self.callbacks.onEntitiesLoadFailure(data);
    }

    function checkGetEntitiesResponse(data, success, failure) {
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                var resp = data.data,
                    entities = resp.entity;

                if (typeof entities !== 'undefined') {
                    success(entities);
                    return;
                } 
        }

        failure(data);
    }

    function getEntities(projectId) {
        self.callbacks.onEntitiesLoadStarted();

        var success = getEntitiesSuccess,
            failure = getEntitiesFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.projectsPath + '/' + projectId + '?expand=entity';

        var authorization = 'Bearer ' + app.user.token;

        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType   : 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkGetEntitiesResponse(data, success, failure);
        });

        getting.fail(function(data) {
            failure(data);
        });
    }

    // TODO: extract to home.js
    function getDocContentAsHTML() {
        // Run a batch operation against the Word object model.
        Word.run(function (ctx) {
            // Create a proxy object for the document body.
            var body = ctx.document.body;
            
            // Queue a commmand to get the HTML contents of the body.
            var bodyHTML = body.getHtml();
            
            // Synchronize the document state by executing the queued-up commands, 
            // and return a promise to indicate task completion.
            return ctx.sync().then(function () {
                app.showNotification("Body HTML contents:", bodyHTML.value);
            }); 
        })
        .catch(function (error) {
            if (error instanceof OfficeExtension.Error) {
                app.showNotification('Erorr:', JSON.stringify(error) + '\nDebug info: ' + JSON.stringify(error.debugInfo));
            }
        });
    }

    self.deinit = function() { 
        console.log('entity deinit');
        deinitWordPadInsertionButtons();
        self.$wordPad = undefined;
    }

    self.initialize = function() {
        self.$wordPad           = $(app.selectors.wordPad);

        self.entities           = undefined;
        self.indexedEntities    = undefined;

        initWordPadInsertionButtons();
    };

    self.getEntities                = getEntities;
    self.isEntitiesLoaded           = isEntitiesLoaded;
    self.clearAutocomplete          = clearAutocomplete;
    self.getAutocompleteSets        = getAutocompleteSets;
    self.initAutocompleteForArticle = initAutocompleteForArticle;
    self.initAutocompleteForProject = initAutocompleteForProject;

    return self;
})();