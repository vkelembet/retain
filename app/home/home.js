
if (typeof Office === 'undefined') {
  Office = {};
  Office.context = {};
  Office.context.requirements = {};
  Office.context.requirements.isSetSupported = function(api, version) {
    return true;
  }
  Office.debug = true;
}

(function() {
  'use strict';

  var dialog;

  // The initialize function must be run each time a new page is loaded
  Office.initialize = function(reason) {
    jQuery(document).ready(
      
      function() {

        app.initialize();

        // Use this to check whether the new API is supported in the Word client.
        if (Office.context.requirements.isSetSupported("WordApi", "1.1")) {
          app.showActivityIndicator();

          inizializeUser();
          initializeTabsUI();

          app.hideActivityIndicator();

          app.showView(app.selectors.signInView);
          addSelectionChangedEventHandler();
        } else {
          app.showNotification('This add-in requires Word 2016 or greater.');
        } 

      }
    );
  };

  function inizializeUser() {
      var cb = app.user.callbacks;

      cb.onSignIn         = onSignIn;

      cb.onSignOutStarted = onSignOutStarted;
      cb.onSignedOut      = onSignedOut;
      cb.onSignOutFailure = onSignOutFailure;

      app.user.initialize();
  }

  function onSignIn() {
    initializeProject();
    initializeArticle();
    initializeEntity();
    initializeAuto();

    app.componentsInitialized = true;

    app.hideActivityIndicator();
    app.showView(app.selectors.signedUserView);
  }

  function onSignOutStarted() {
    console.log('onSignOutStarted');
  } 

  function onSignedOut() {
    deinit();
    app.showView(app.selectors.signInView);
  }

  function onSignOutFailure(data) {
    deinit();
    app.showView(app.selectors.signInView);
  }

  function deinit() {
    if (app.project.isInitialized()) {
      app.project.deinit();
      app.article.deinit();
      app.entity.deinit();
    }
  }

  function showNotificationWithLinkToArticleByRef(ref) {
    var admin   = app.environment.admin,
        href    = admin.rootURL + '/' + admin.articlePath + '/' + ref.articleId,
        header  = 'Go to referenced document in admin panel:',
        positionHtml = '<br>(page: ' + ref.pageNumber + ', line: ' + ref.lineNumber + ')',
        html    = linkHtml(href, href, ref.title) + positionHtml;

    app.notify({
      header: header,
      html: html
    });
  }

  function onNetworkFailure(data) {
    // Unathorized.
    if (data.status == 401) {
      deinit();
      app.showView(app.selectors.signInView);
    }
  }

  function isUserSignedIn() {
    return (typeof app.user.details.email !== 'undefined');
  }

  function getSelectedProject() {
    return app.project.selectedProject || {};
  }

  function getSelectedArticle() {
    return app.article.selectedArticle || {};
  }

  function showArticleContentTab() {
    showTabByClassName('pivot-link-article-content');
  }

  function showTabByClassName(className) {
    var selector        = '.' + className,
        $pivotContainer = $('.ms-Pivot'),
        $pivotLink      = $pivotContainer.find(selector);

    var event = $.Event('click');
    event.target = $pivotLink.get(0);

    $pivotContainer.trigger(event);
  }

  function isArticleListLoadedForProject(projectId) {
    var articles = app.article.articlesContext.articles;
    return (articles.length > 0) && (articles[0].project_id === projectId);
  }

  function navigateToArticleByRef(ref) {
    var projectId = getSelectedProject().id,
        articleId = getSelectedArticle().id;

    // Specify position to which to scroll after loading article content.
    app.article.setScrollPosition(ref.pageNumber, ref.lineNumber);
    app.article.navigateToArticleWithId = ref.articleId;

    if (ref.projectId === projectId) {
      // Referenced project are the same as opened.

      if (ref.articleId === articleId) {
        // Referenced article already loaded.
        showArticleContentTab();
      } else {
        // Different article that opened at the moment.
        app.article.selectArticleByNavigation(ref.articleId);
      }

    } else {
      // Referenced project should be loaded first.

      var refProject = app.project.getProjectById(ref.projectId);

      if (refProject !== null) {

        app.project.selectProjectByNavigation(ref.projectId);

      } else {

        app.notify({
          header: 'Referenced project not found!',
          text: 'Project id: ' + ref.projectId
        });

      }

    }
  }

  function isReferencedArticleCreateBySignedInUser(ref) {
    return app.user.details.id === ref.userId;
  }

  function onArticleRefCCSelected(ref) {
    if (isUserSignedIn()) {

      if (isReferencedArticleCreateBySignedInUser(ref)) {
        
        // Disable notification temporarily.
        //showNotificationWithLinkToArticleByRef(ref);
        
        navigateToArticleByRef(ref);

      } else {

        app.notify({
          header: 'Oops!',
          text: 'This document reference created by another user'
        });

      }

    } else {

      app.notify({
        header: 'Oops!',
        text: 'Please sign in first in order to use document references'
      });

    }
  }

  function initializeProject() {
    var cb = app.project.callbacks;

    cb.onProjectSelected    = onProjectSelected;
    cb.onProjectDeselected  = onProjectDeselected;

    app.project.initialize();
  }

  function initializeArticle() {
    var cb = app.article.callbacks;

    cb.onArticleListLoadStarted     = onArticleListLoadStarted;
    cb.onArticleListLoaded          = onArticleListLoaded;
    cb.onArticleListLoadFailure     = onArticleListLoadFailure;

    cb.onArticleSelected            = onArticleSelected;
    cb.onArticleDeselected          = onArticleDeselected;

    cb.onArticleContentLoadStarted  = onArticleContentLoadStarted;
    cb.onArticleContentLoaded       = onArticleContentLoaded;
    cb.onArticleContentLoadFailure  = onArticleContentLoadFailure;
    cb.onArticleContentRendered     = onArticleContentRendered;

    cb.onInsertArticleSelectedText        = onInsertArticleSelectedText;
    cb.onInsertArticleSelectedTextAndRef  = onInsertArticleSelectedTextAndRef;
    cb.onInsertArticleRef                 = onInsertArticleRef;

    app.article.initialize();
  }

  function initializeEntity() {
    var cb = app.entity.callbacks;

    cb.onEntitiesLoadStarted  = onEntitiesLoadStarted;
    cb.onEntitiesLoaded       = onEntitiesLoaded;
    cb.onEntitiesLoadFailure  = onEntitiesLoadFailure;

    cb.onSendStyledTextToWordDocument = onSendStyledTextToWordDocument;

    app.entity.initialize();
  }

  function initializeAuto() {
    app.auto.initialize();
  }

  function resetHighliterAfterDelay() {
    var duration = 1000; // 1 sec
    setTimeout(function() {
      app.article.setHighlighterState('reset');
    }, duration);
  }

  function serializeNodesAsHtmlString(nodes) {
    var $div = $(document.createElement('div'));
    $div.append(nodes);
    return $div.html();
  }

  function onEntitiesLoadStarted() {
    app.showActivityIndicator();
  }

  function onEntitiesLoaded(entities) {
    var articleId = getSelectedArticle().id,
        projectId = getSelectedProject().id;
    
    if (typeof articleId !== 'undefined') {

      app.entity.initAutocompleteForArticle(articleId);
      var sets = app.entity.getAutocompleteSets();
      app.article.setHighlightAutocompleteSets(sets);

    } else if (typeof projectId !== 'undefined') {

      app.entity.initAutocompleteForProject();
      var sets = app.entity.getAutocompleteSets();
      app.article.setHighlightAutocompleteSets(sets);

    } else {

      app.entity.clearAutocomplete();

    }

    app.hideActivityIndicator();
  }

  function onEntitiesLoadFailure(data) {
    app.entity.clearAutocomplete();
    app.hideActivityIndicator();
    app.showNotification('Unable to get project entities', '');
    onNetworkFailure(data);
  }

  function onArticleSelected(article) {
    pdf2htmlEX.defaultViewer = undefined;
    
    app.article.getArticleContent(article.uuid); 

    if (app.entity.isEntitiesLoaded()) {
      app.entity.initAutocompleteForArticle(article.id);
    }
  }

  function onArticleDeselected() {
    pdf2htmlEX.defaultViewer = undefined;

    app.entity.initAutocompleteForProject();
  }

  function onArticleListLoadStarted() {
    app.showActivityIndicator();
  }

  function onArticleListLoaded() {
    var articleId = app.article.navigateToArticleWithId;

    if (articleId !== null) {
      app.article.selectArticleByNavigation(articleId);
    }

    app.hideActivityIndicator();
  }

  function onArticleListLoadFailure(data) {
    app.hideActivityIndicator();
    app.showNotification('Unable to get documents', '');
    onNetworkFailure(data);
  }

  function onArticleContentLoadStarted() {
    app.showActivityIndicator();
    app.article.clearArticleContent();
  }

  function onArticleContentLoaded(articleContent) {
    app.article.clearArticleContent();
    app.article.renderArticleContent(articleContent);
  }

  function onArticleContentLoadFailure(data) {
    app.hideActivityIndicator();
    app.article.clearArticleContent();
    app.showNotification('Error', 'Unable to get document content');
    onNetworkFailure(data);
  }

  function onArticleContentRendered() {
    var articleId = getSelectedArticle().id;

    if (app.article.navigateToArticleWithId !== null) {
      showArticleContentTab();
    }

    app.hideActivityIndicator();
  }

  function onArticleContentTabOpened() {
    if (typeof pdf2htmlEX.defaultViewer === 'undefined') {
      tryToUseRenderedArticle();
    } else {
      fitAndScroll();
    }
  }

  function tryToUseRenderedArticle() {
    var $container = $('#page-container');
    
    if ($container.length > 0 && $container.get(0).clientWidth) {
      app.article.setArticleContentRendered();
      app.article.initArticleContentHandlers();

      initDefaultViewer();
      
      //app.article.resetScrollPosition();
      
      // TODO: check if navigated to position
      fitAndScroll();
    } else {
      // Wait until page container is visible.
      window.requestAnimationFrame(tryToUseRenderedArticle);
    }
  }

  function initDefaultViewer() {
    pdf2htmlEX.defaultViewer = new pdf2htmlEX.Viewer({ });
    pdf2htmlEX.defaultViewer.init_after_loading_content();
    
    $('#sidebar').removeClass('opened');
    $(window).resize(app.article.fitArticleContent);
  }

  function fitAndScroll() {
    var article = app.article;

    article.fitArticleContent();
    article.scrollToPosition();
    article.navigateToArticleWithId = null;
  }

  function addTrailingSpace(toString) {
    var str = toString;

    if (str != '') {
      var space = ' ';
      
      if (!str.endsWith(space)) {
        str = str + space;
      }
    }

    return str;
  }

  function wrapStringWithChar(stringToWrap, charToWrapWith) {
    var str = stringToWrap,
        char = charToWrapWith;

    if (str != '') {
      str = char + str + char;
    }

    return str;
  }

  // TODO: unused?
  function wrapTextInSpaces(textToInsert) {
    var space = ' ';

    if (textToInsert != '') {
      if (!textToInsert.startsWith(space)) {
        textToInsert = space + textToInsert;
      }

      if (!textToInsert.endsWith(space)) {
        textToInsert = textToInsert + space;
      }
    }

    return textToInsert;
  }

  function onInsertArticleSelectedText(textToInsert) {
    insertText(addTrailingSpace(textToInsert));
  }

  function onInsertArticleSelectedTextAndRef(textToInsert, ref) {
    var char = '"',
        text = addTrailingSpace(wrapStringWithChar(textToInsert.trim(), char));
    insertTextAndArticleRefCC(text, ref);
  }

  function onInsertArticleRef(ref) {
    insertArticleRefCC(ref);
  }

  function createRefContentControl(range, ref) {
    var contents = [ref.title], 
        paras = $.trim(ref.paragraphs);

    if (ref.pageNumber != 1 || ref.lineNumber != 1) {
      contents.push('page: ' + ref.pageNumber);
      contents.push('line: ' + ref.lineNumber);
    }    

    if (paras && paras !== '') {
      contents.push(paras);
    }
       
    var text = contents.join(', '),
        tag = '<b><i>(' + text + ')</i></b>';

    // Queue a commmand to create the content control.
    var cc = range.insertContentControl();

    cc.tag = JSON.stringify(ref);
    cc.title = app.contentControl.title;
    cc.style = 'Normal';
    cc.insertHtml(tag, Word.InsertLocation.start);
    cc.cannotEdit = true;
    cc.appearance = 'boundingBox';

    return cc;
  }

  function insertTextAndArticleRefCC(text, ref, callback) {
    callback = callback || function() {};

    Word.run(function(context) {
      var selectionRange = context.document.getSelection();

      return isRangeInsideContentControl(context, selectionRange, function() {
        // Inside CC
        notifyIsInsideCC();
      }, function() {
        // Outside CC
        var htmlToInsert = '<i>' + text + '</i>';
        
        // Insert selected text
        selectionRange.insertHtml(htmlToInsert, Word.InsertLocation.end);
        
        // Insert content control placeholder
        var ccPlaceholderRange = selectionRange.insertText('[cc]', Word.InsertLocation.end);
        
        // Insert end placeholder to select it later
        var endRange = selectionRange.insertText(' ', Word.InsertLocation.end);
        
        // Add new line before CC placeholder
        ccPlaceholderRange.insertBreak(Word.BreakType.line, Word.InsertLocation.before);
        
        // Remove CC placeholder text
        ccPlaceholderRange.clear();
        
        // Wrape CC placeholder with content control
        createRefContentControl(ccPlaceholderRange, ref);

        // Move selection to the end
        endRange.select(Word.InsertLocation.end);

        // Remove end placeholder text
        //endRange.clear();

        return context.sync().then(callback);
      });

    }).catch(handleAsyncError);
  }

  function insertArticleRefCC(ref) {
    Word.run(function (context) {
      var selectionRange = context.document.getSelection();

      return isRangeInsideContentControl(context, selectionRange, function() {
        // Inside CC
        notifyIsInsideCC();
      }, function() {
        // Outside CC
        selectionRange.clear();

        // Insert content control placeholder
        var ccPlaceholderRange = selectionRange.insertText('[cc]', Word.InsertLocation.end);

        // Insert end placeholder to select it later
        var endRange = selectionRange.insertText(' ', Word.InsertLocation.end);

        // Remove CC placeholder text
        ccPlaceholderRange.clear();

        // Wrape CC placeholder with content control
        createRefContentControl(ccPlaceholderRange, ref);

        // Move selection to the end
        endRange.select(Word.InsertLocation.end);

        return context.sync().then(callback);
      });
    }).catch(handleAsyncError);
  }

  function handleAsyncSuccess() {
    console.log('Success!');
  }

  function handleAsyncError(error) {
    if (error instanceof OfficeExtension.Error) {
      var msg = JSON.stringify(error) + '\nDebug info: ' + JSON.stringify(error.debugInfo);
      app.showNotification('Erorr:', msg);
    }
  }

  function onProjectSelected(project) {
    app.article.clearArticleContent();

    if (project !== null) {

      app.article.getArticles(project.id);
      app.entity.getEntities(project.id);

    } else {

      app.article.renderEmptyArticleList();
      app.entity.clearAutocomplete();
      
      app.showNotification('Error', 'No project data retrieved');

    }
  }

  function onProjectDeselected() {
    app.article.clearArticleList();
    app.article.clearArticleContent();
    app.entity.clearAutocomplete();
  }

  function initializeTabsUI() {
    if ($.fn.Pivot) {
      var $pivotContainer = $('.ms-Pivot');

      $pivotContainer.Pivot();
          
      $pivotContainer.on('click', '.ms-Pivot-link', function(event) {
        event.preventDefault();

        var $pivotLink = $(this);

        // Unwrap selected nodes, deselect ites, hide callout.
        app.article.clearSelection();

        switchToPivot($pivotLink);
      });
    }
  }

  function switchToPivot($pivotLink) {
    var $pivotContainer         = $('.ms-Pivot'),
        pivot                   = $pivotContainer.Pivot(),
        $links                  = $pivotContainer.children('.ms-Pivot-link'),
        pivotContainerId        = '#' + $pivotContainer.data('container-id'),
        $pivotContentContainer  = $(pivotContainerId),
        $tabs                   = $pivotContentContainer.children('.pivot-tab');

    // Get index of selected pivot.
    var index = $links.index($pivotLink);

    // Show content block accordingly.
    $tabs.removeClass('is-selected slide-left');
    $tabs.eq(index).addClass('is-selected slide-left');

    if ($pivotLink.hasClass('pivot-link-article-content')) {
      onArticleContentTabOpened();
    }
  }

  function onSendStyledTextToWordDocument(text, style) {
    insertParagraphWithStyle(text, style);
  }

  // https://github.com/OfficeDev/Word-Add-in-DocumentAssembly/blob/master/WordAPIDocAssemblySampleWeb/App/Home/Home.js#L172
  function insertParagraphWithStyle(text, wordStyle) {
    Word.run(function (context) {
        // Create a range proxy object for the current selection.
        var selection = context.document.getSelection();

        return isRangeInsideContentControl(context, selection, function() {
          // Inside CC
          notifyIsInsideCC();
        }, function() {
          // Outside CC
          var textWithBreaks = text.replace(/\n/g, '<br>');

          // Create paragraph.
          var paragraph = selection.insertParagraph('', Word.InsertLocation.after);
          paragraph.style = wordStyle;

          paragraph.insertHtml(textWithBreaks, Word.InsertLocation.end);

          // Select inserted paragraph.
          paragraph.select();

          return context.sync().then(function () { }).catch(handleAsyncError);
        }); 
    });
  }

  function setSelectedText(text) {
    Office.context.document.setSelectedDataAsync(text, {
        coercionType: Office.CoercionType.Text
    }, function (asyncResult) {
        var error = asyncResult.error;
        if (asyncResult.status === Office.AsyncResultStatus.Failed) {
            app.showNotification(
              'Unable to insert selected text', 
              error.name + ': ' + error.message
            );
        }
    });
  }

  // Used for debugging
  function objToString (obj) {
      var str = '';
      for (var p in obj) {
          if (obj.hasOwnProperty(p)) {
              str += p + '::' + obj[p] + '\n';
          }
      }
      return str;
  }

  // Workaround due to inconsistent parentContentControl API:
  // see https://github.com/OfficeDev/office-js-docs/blob/master/reference/word/range.md
  function isRealContentControl(contentControl) {
      if (contentControl && contentControl.type == 'RichText') {
        return true
      }

      return false;
  }

  function isRangeInsideContentControl(context, range, insideContentControlCallback, outsideContentControlCallback) {
      var parentCC = range.parentContentControl;
      context.load(parentCC);

      return context.sync().then(function() {
        if (isRealContentControl(parentCC)) {
          insideContentControlCallback();
        } else {
          outsideContentControlCallback();
        }
      });
  }

  function insertText(text, callback) {
    callback = callback || function() {};

    // Run a batch operation against the Word object model.
    Word.run(function (context) {
      
        // Create a range proxy object for the current selection.
        var selection = context.document.getSelection();

        return isRangeInsideContentControl(context, selection, function() {
          // Inside CC
          notifyIsInsideCC();
          callback(context);
        }, function() {
          // Outside CC
          var range = selection.insertText(text, Word.InsertLocation.end);
          context.trace('insertText successful');
          
          return context.sync().then(function() {
            range.select("End");

            return context.sync().then(function() {
              callback(context);
            });
          }); 
        });

    }).catch(function() {});
    //handleAsyncError
  }

  function notifyIsInsideCC() {
    app.notify({
      header: "Oops!",
      text: "Looks like you're in content control. Move selection outside and retry."
    });
  }

  function linkHtml(href, title, text) {
    return "<a href=\"" + href + "\" title=\"" + title + "\">" + text + "</a>";
  }

  function addSelectionChangedEventHandler() {
    var document  = Office.context.document,
        eventType = Office.EventType.DocumentSelectionChanged;

    document.addHandlerAsync(eventType, selChangedHandler);
  }

  function selChangedHandler(eventArgs) {
    var document = eventArgs.document;

    Word.run(function (context) {
      var cc = context.document.getSelection().parentContentControl;
      context.load(cc);

      return context.sync().then(function () {
        if (cc !== null && cc.title === app.contentControl.title) {
          var ref = JSON.parse(cc.tag);
          onArticleRefCCSelected(ref);
        }
      })

    }).catch(function() {
      // Do nothing.
    });
  }

  /*
    Debug functions.
  */

  // Reads data from current document selection and displays a notification
  function getDataFromSelection() {
    var document = Office.context.document;

    document.getSelectedDataAsync(Office.CoercionType.Text,
      function(result) {
        if (result.status === Office.AsyncResultStatus.Succeeded) {
          app.showNotification('The selected text is:', '"' + result.value + '"');
        } else {
          app.showNotification('Error:', result.error.message);
        }
      }
    );
  }

  function showWordAtSelection() {

    Word.run(function (context) {
      var word = context.document.getSelection().getTextRanges([' '], true).first;
      
      context.load(word, 'text');
      
      return context.sync().then(function () {
          app.showNotification('Word: ' + word.text);
      })

    }).catch(function (error) {
      var msg = 'Error:' + '\n' + JSON.stringify(error);

      if (error instanceof OfficeExtension.Error) {
        msg = msg + '\n' + JSON.stringify(error.debugInfo);
      }

      app.showNotification(msg);
    });

  }

  /**
   * Shows how to get the sentence of the current selection.
   **/
  function showSentenceAtSelection() {

      Word.run(function (context) {

          // This is the range of the sentence we want to save.
          // You can just add the cursor to the sentence.
          var sentence = context.document.getSelection().getTextRanges(['.'], true).first;

          // Queue a command to load the sentence text.
          context.load(sentence, 'text');

          // Synchronize the document state by executing the queued commands,
          // and return a promise to indicate task completion.
          return context.sync()
              .then(function () {

                  // Here we save the sentence text as boilerplate in
                  // local storage. The text is saved without formatting.
                  // var sentenceName = document.getElementById('inputAddBoilerplateSentence').value;
                  // saveBoilerplate(sentenceName, sentence.text, 'sentence');
                  app.showNotification('Sentence text: ' + sentence.text);
              })
      }).catch(function (error) {
          var msg = 'Error:' + '\n' + JSON.stringify(error);

          if (error instanceof OfficeExtension.Error) {
            msg = msg + '\n' + JSON.stringify(error.debugInfo);
          }

          app.showNotification(msg);
      });
  }

    function dialogCallback(asyncResult){ 
      dialog = asyncResult.value; 
      dialog.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogMessageReceived, messageHandler); 
      dialog.addEventHandler(Microsoft.Office.WebExtension.EventType.DialogEventReceived,  eventHandler); 
  } 

  function messageHandler(arg){ 
      actOnMessage(arg.message); 
  } 

  function eventHandler(arg){ 
      actOnEvent(arg.message);
      dialog.close(); 
  } 

  function openDialog() {
      Office.context.ui.displayDialogAsync("./dialog.html",  
          { height: 80, width: 50 }, dialogCallback); 
  }

})(); 

// debug
if (Office.debug == true) {
  Office.initialize();
}
