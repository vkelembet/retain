app.note = (function() { 
    'use strict';

    var self = {
        $notesList: undefined,

        $insertNoteTextButton: undefined,
        $insertNoteTextWithRefButton: undefined,
        $insertNoteRefButtonx: undefined,

        notes: undefined,
        indexedNotes: {},

        context: { 
            notes: {},
            articles: {}, 
            tags: [] 
        },

        callbacks: {
            onNotesLoadStarted:             function() { },
            onNotesLoaded:                  function() { },
            onNotesLoadingFailure:          function() { },

            // TODO: implement after SCRUM-90 is fixed
            onHighlightCreate:              function() { },
            onHighlightCreateStarted:       function(note) { },
            onHighlightCreated:             function(note) { },
            onHighlightCreateFailure:       function(data) { },

            onNoteCreateStarted:            function(note) { },
            onNoteCreated:                  function(note) { },
            onNoteCreateFailure:            function(data) { },

            onInsertNoteTextClick:          function(selectedNote) { },
            onInsertNoteTextAndRefClick:    function(selectedNote) { },
            onInsertNoteRefClick:           function(selectedNote) { }
        }
    };

    function isNotesLoaded() {
        var notes = self.notes;
        if (typeof notes !== 'undefined' && notes.constructor === Array) {
            return true;
        }
        return false;
    }

    function setTags(tagsIndexedById) {
        self.context.tags = tagsIndexedById;
    }

    function getNotesSuccess(notes) {
        self.notes        = notes;
        self.indexedNotes = createNotesIndex(notes);
        self.callbacks.onNotesLoaded(notes);
    }

    function getNotesFailure(data) {
        self.notes          = undefined;
        self.indexedNotes   = {};
        self.callbacks.onNotesLoadingFailure(data);
    }

    function checkGetNotesResponse(data, success, failure) {
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                
                var notes = data.data;
                success(notes);
                return;
        }

        failure(data);
    }

    function getNotes(projectId) {
        self.callbacks.onNotesLoadStarted();

        var success = getNotesSuccess,
            failure = getNotesFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.notesPath + '?project_id=' + projectId;

        var authorization = 'Bearer ' + app.user.token;

        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkGetNotesResponse(data, success, failure);
        });

        getting.fail(function(data) {
            failure(data);
        });
    }

    function createHighlightNoteSuccess(note) {
        self.note = note; // (?)
        self.callbacks.onHighlightNoteCreated(note);
    }

    function createHighlightNoteFailure(data) {
        self.note = undefined; // (?)
        self.callbacks.onHighlightNoteCreateFailure(data);
    }

    function checkCreateHighlightNoteResponse(data, success, failure) {
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                var note = data.data;

                if (typeof note !== 'undefined') {
                    success(note);
                    return;
                } 
        }

        failure(data);
    }

    function createHighlightNote(note) {
        self.callbacks.onHighlightCreateStarted(note);
 
        console.log('createHighlightNote', note);

        var success = createHighlightNoteSuccess,
            failure = createHighlightNoteFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.notesPath;

        var authorization = 'Bearer ' + app.user.token;

        /*
            note: {
                "text": "selected article text",
                "html": null,
                "note": null,
                "date": null,
                "doc_id": 106,
                "tag_id": null,
                "user_id": 1,
                "page_number": null,
                "line_number": null,
                "paragraph_number": null,
                "positions": "{pageStart:1,pageEnd:1,lineStart:1,lineEnd:1}"
            }
        */
        var data = JSON.stringify(note);

        var posting = $.ajax({ 
            method: 'post',
            url: url,
            contentType: 'application/json',
            dataType:    'json',
            data: data,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        posting.done(function(data) {
            checkCreateHighlightNoteResponse(data, success, failure);
        });

        posting.fail(function(data) {
            failure(data);
        });
    }


    function createNote(note) {
        self.callbacks.onNoteCreateStarted(note);

        var success = createNoteSuccess,
            failure = createNoteFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.notesPath;

        var authorization = 'Bearer ' + app.user.token;

        /*
            note: {
                "text": "note text",
                "html": null,
                "note": null,
                "date": null,
                "doc_id": 106,
                "tag_id": 28,
                "user_id": 1,
                "page_number": null,
                "line_number": null,
                "paragraph_number": null,
                "positions": null
            }
        */
        var data = JSON.stringify(note);

        var posting = $.ajax({ 
            method: 'post',
            url: url,
            contentType: 'application/json',
            dataType:    'json',
            data: data,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        posting.done(function(data) {
            checkCreateNoteResponse(data, success, failure);
        });

        posting.fail(function(data) {
            failure(data);
        });
    }

    function checkCreateNoteResponse(data, success, failure) {
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                var note = data.data;

                if (typeof note !== 'undefined') {
                    success(note);
                    return;
                } 
        }

        failure(data);
    }

    function createNoteSuccess(note) {
        self.note = note; // (?)
        self.callbacks.onNoteCreated(note);
    }

    function createNoteFailure(data) {
        self.note = undefined; // (?)
        self.callbacks.onNoteCreateFailure(data);
    }

    function clearNotes() {
        self.notes = undefined;
        self.indexedNotes = {};

        self.context.notes = {};
        self.context.articles = {};
        self.context.tags = [];
    }

    function renderNotes() {
        
        var notes = self.context.notes;
        var notesExists = $.isPlainObject(notes) && !$.isEmptyObject(notes);
        
        if (notesExists) {
            renderNotesList();
        } else {
            renderEmptyNotesList();
        }
    }

    function renderNotesList() {
        var template = app.templates.notesList( self.context );
        self.$notesList.html( template );
    }

    function renderEmptyNotesList() {
        clearNotes();
        var template = app.templates.emptyNotesList({ });
        self.$notesList.html( template );
    }

    function clearNotesList() {
        clearNotes();
        var template = app.templates.notesListPlaceholder({ });
        self.$notesList.html( template );
    }

    function initNotesListHandlers() {
        var $list = self.$notesList;
        
        $list.on('click', '.insert-note-text', function(e) {
            var noteId = $(this).data('id'),
                selectedNote = getNoteBy(noteId);

            self.callbacks.onInsertNoteTextClick(selectedNote);
        });

        $list.on('click', '.insert-note-text-and-article-ref', function(e) {
            var noteId = $(this).data('id'),
                selectedNote = getNoteBy(noteId);

            self.callbacks.onInsertNoteTextAndRefClick(selectedNote);
        });

        $list.on('click', '.insert-article-ref', function(e) {
            var noteId = $(this).data('id'),
                selectedNote = getNoteBy(noteId);

            self.callbacks.onInsertNoteRefClick(selectedNote);
        });
    }

    function deinitNotesListHandlers() {
        var $list = self.$notesList;

        $list.off('click');
    }

    function getNoteBy(id) {
        var notes   = self.notes;
        // TODO: use note index
        var result  = $.grep(notes, function(e) { return e.id == id; });

        if (result.length == 0) {
            // not found
            return null;
        } else if (result.length == 1) {
            // access the property using result[0]
            return result[0];
        } else {
            // multiple items found
            return null;
        }
    }

    function getArticleNotes(articleId) {
        var articleNotes = self.indexedNotes[articleId];

        var notesExists = (typeof articleNotes !== 'undefined') && (articleNotes.length > 0);
        if (notesExists) {
            return articleNotes;
        }

        return null;
    }

    function getProjectNotes(projectId) {
        var projectNotes = self.indexedNotes;

        var notesExists = $.isPlainObject(projectNotes) && !$.isEmptyObject(projectNotes);
        if (notesExists) {
            return projectNotes;
        }

        return null;
    }

    function getProjectArticles() {
        return app.article.getArticleIndex();
    }

    function getArticlesBy(articleId) {
        var articles = { };
        
        var article = app.article.getArticleBy(articleId);
        if (typeof article !== 'undefined') {
            articles[articleId] = article;
        }

        return articles;
    }

    function showNotesForProject(projectId) {
        var notes = getProjectNotes(projectId);
        
        if (notes !== null) {
            self.context.notes = notes;

            var articles = getProjectArticles();
            self.context.articles = articles;
        } else {
            clearNotes();
        }

        renderNotes();
    }

    function showNotesForArticle(articleId) {
        var articleNotes = getArticleNotes(articleId);
        
        var notes = {};
        notes[articleId] = articleNotes;

        if (articleNotes !== null) {
            self.context.notes = notes;

            var articles = getArticlesBy(articleId);
            self.context.articles = articles;
        } else {
            clearNotes();
        }

        renderNotes();
    }

    function createNotesIndex(notes) {
        var len = notes.length, 
            i, 
            note, 
            articleId,
            indexedNotes = {};

        for (i = 0; i < len; ++i) {
            note = notes[i];
            articleId = note.doc_id;

            if (typeof indexedNotes[articleId] == 'undefined') {
                indexedNotes[articleId] = [];
            }

            indexedNotes[articleId].push(note);
        }

        return indexedNotes;
    }

    function getHighlightNotes(articleId) {
        console.log("getHighlightNotes articleId:", articleId);

        var notes = self.indexedNotes[articleId]; // by articleId
        if (typeof notes == 'undefined') {
            return notes; // null?
        }

        // Assume all notes without tag are highlight notes.
        var highlightNotes = $.grep(notes, function(note) { 
            return note.tag_id == null;
        });

        return highlightNotes;
    }

    self.deinit = function() { 
        deinitNotesListHandlers();

        self.notes          = [];
        self.indexedNotes   = {};

        clearNotesList();
        self.$notesList     = undefined;
    }

    self.initialize = function() {
        self.$notesList     = $(app.selectors.notesList);

        self.notes          = [];
        self.indexedNotes   = {};

        clearNotesList();
        initNotesListHandlers();
    };

    self.setTags                = setTags;
    self.getNotes               = getNotes; // TODO: rename to load
    self.createNote             = createNote;
    self.clearNotes             = clearNotes;
    self.isNotesLoaded          = isNotesLoaded;
    self.getArticleNotes        = getArticleNotes;
    self.clearNotesList         = clearNotesList;
    self.getHighlightNotes      = getHighlightNotes;
    self.showNotesForArticle    = showNotesForArticle;
    self.createHighlightNote    = createHighlightNote;
    self.showNotesForProject    = showNotesForProject;
    self.renderEmptyNotesList   = renderEmptyNotesList;

    return self;
})();