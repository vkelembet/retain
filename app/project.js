app.project = (function() {  // jshint ignore:line
    'use strict';

    var self = {
        initialized: false,

        $projectsDropdown: undefined,

        projectsContext: { projects: [] },
        projects: undefined,
        selectedProject: undefined,

        callbacks: {
            onProjectSelected: function() {},
            onProjectDeselected: function() {}
        }
    };

    function initProjectsDropdown() {
        var $select = self.$projectsDropdown.find('.ms-Dropdown-select');
        $select.on('change', onProjectSelected);
        enableProjectsDropdown();
    }

    function deinitProjectsDropdown() {
        var $select = self.$projectsDropdown.find('.ms-Dropdown-select');
        $select.off('change');
    }

    function onProjectSelected(e) {
        var projectId = this.value;

        if (projectId == 'default') {
            self.selectedProject = undefined;
            self.callbacks.onProjectDeselected();
        } else {
            var project = getProjectById(projectId);
            self.selectedProject = project;
            self.callbacks.onProjectSelected(project);
        }
    }

    function selectProjectByNavigation(projectId) {
        pdf2htmlEX.defaultViewer = undefined;
        
        // TODO: to plugin
        var $dropdown = self.$projectsDropdown;
        var $option = $dropdown.find('option[value=' + projectId + ']');
        var optionText = $option.text();

        $dropdown.find(".ms-Dropdown-item").each(function(index) {
            if (optionText === $(this).text()) {
                $(this).click();
            }
        });
    }

    function getProjectById(id) {
        var project = self.projects[id];

        if (typeof project !== 'undefined') {
            return project;
        } else {
            return null;
        }
    }

    function getProjectsIndexedById(projects) {
        var indexedProjects = {}, 
            project, i, len = projects.length;

        for (i = 0; i < len; i++) {
            project = projects[i];
            indexedProjects[project.id] = project;
        }

        return indexedProjects;
    }

    function getProjectsSuccess(projects) {
        self.projectsContext.projects = projects;
        self.projects = getProjectsIndexedById(projects);
        
        renderProjectsDropdown();
        
        if (projects.length > 0) {
            initProjectsDropdown();
        } else {
            disableProjectsDropdown();
            // TODO: show notification with action
            app.showNotification('Oops!', "You don't have any projects yet.");
        }
    }

    function getProjectsFailure(data) {
        app.showNotification('Unable to get projects', JSON.stringify(data));
    }

    function checkGetProjectsResponse(data, success, failure) {
        if (typeof data !== 'undefined') {
            var projects = data.data;

            if (data.success == true && typeof projects !== 'undefined') {
                success(projects);
            } else {
                failure(data);
            }
        } else {
            failure(data);
        }
    }

    function getProjects(success, failure) {
        var env = app.environment,
            url = env.rootURL + '/' + env.projectsPath;

        var authorization = 'Bearer ' + app.user.token;

        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType   : 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkGetProjectsResponse(data, success, failure);
        });

        getting.fail(function(data) {
            failure(data);
        });

        // debug fake projects
        // success([
        //     {
        //         "id": 40,
        //         "title": "Intelliarts project"
        //     }
        // ]);

        // debug no projects yet
        //success([]);
    }

    function renderProjectsDropdown() {
        var template = app.templates.projectsDropdown(self.projectsContext);
        self.$projectsDropdown.html(template);

        if ($.fn.Dropdown) {
            self.$projectsDropdown.find('.ms-Dropdown').Dropdown();
        }
    }

    function enableProjectsDropdown() {
        self.$projectsDropdown.find('.ms-Dropdown').removeClass('is-disabled');
    }

    function disableProjectsDropdown() {
        self.$projectsDropdown.find('.ms-Dropdown').addClass('is-disabled');
    }

    function isInitialized() {
        return self.initialized;
    }

    self.deinit = function() { 
        console.log('project deinit');
        deinitProjectsDropdown();
        self.projectsContext.projects = [];
        self.$projectsDropdown = undefined;

        self.initialized = false;
    }

    self.initialize = function() {
        self.$projectsDropdown = $(app.selectors.projectsDropdown);
        self.projectsContext.projects = [];

        renderProjectsDropdown();
        disableProjectsDropdown();

        getProjects(getProjectsSuccess, getProjectsFailure);

        self.initialized = true;
    };

    self.getProjectById             = getProjectById;
    self.isInitialized              = isInitialized;
    self.selectProjectByNavigation  = selectProjectByNavigation;

    return self;
})();

