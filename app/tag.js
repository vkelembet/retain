app.tag = (function() { 
    'use strict';

    var self = {
        tags: undefined,
        indexedTags: {},

        callbacks: {
            onTagsLoadStarted: function() { },
            onTagsLoaded: function(tags) { },
            onTagsLoadFailure: function(data) { },
            
            onTagCreateStarted: function() { },
            onTagCreated: function() { },
            onTagCreateFailure: function() { }
        }
    };

    function isTagsLoaded() {
        var tags = self.tags;
        if (typeof tags !== 'undefined' && tags.constructor === Array) {
            return true;
        }
        return false;
    }

    function clearTags() {
        self.tags = undefined;
        self.indexedTags = {};
    }

    function checkGetTagsResponse(data, success, failure) {
        console.log('tags data', data);
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                var tags = data.data;

                if (typeof tags !== 'undefined') {
                    success(tags);
                    return;
                } 
        }

        failure(data);
    }

    function getTagsSuccess(tags) {
        self.tags           = tags;
        self.indexedTags    = getIndexedTags(tags);

        self.callbacks.onTagsLoaded(tags);
    }

    function getIndexedTags(tags) {
         var len = tags.length, 
            i, 
            tag, 
            indexedTags = {};

        for (i = 0; i < len; ++i) {
            tag = tags[i];
            indexedTags[tag.id] = tag;
        }

        return indexedTags;       
    }

    function getTagsFailure(data) {
        clearTags();
        self.callbacks.onTagsLoadFailure(data);
    }

    // TODO: rename get to load
    function getTags() {
        self.callbacks.onTagsLoadStarted();

        var success = getTagsSuccess,
            failure = getTagsFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.tagsPath;

        var authorization = 'Bearer ' + app.user.token;

        // TODO: facade
        var getting = $.ajax({ 
            method: 'get',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            data: null,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        getting.done(function(data) {
            checkGetTagsResponse(data, success, failure);
        });

        getting.fail(function(data) {
            failure(data);
        });
    }

    function getTagsIndexedById() {
        return self.indexedTags;
    }

    function createTag(title) {
        self.callbacks.onTagCreateStarted();

        var success = createTagSuccess,
            failure = createTagFailure,
            env = app.environment,
            url = env.rootURL + '/' + env.tagsPath;

        var authorization = 'Bearer ' + app.user.token;

        var data = JSON.stringify({ 
            title: title
        });

        var posting = $.ajax({ 
            method: 'post',
            url: url,
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            headers: {
                'Authorization': authorization
            },
            crossDomain: true
        });

        posting.done(function(data) {
            checkCreateTagResponse(data, success, failure);
        });

        posting.fail(function(data) {
            failure(data);
        });
    }

    function checkCreateTagResponse(data, success, failure) {
        if (typeof data !== 'undefined' &&
            typeof data.data !== 'undefined' &&
            data.success == true) {
                var createdTag = data.data;

                if (typeof createdTag !== 'undefined') {
                    success(createdTag);
                    return;
                } 
        }

        failure(data);    
    }

    function createTagSuccess(createdTag) {
        self.callbacks.onTagCreated(createdTag);
    }

    function createTagFailure(data) {
        self.callbacks.onTagCreateFailure(data);
    }

    self.deinit = function() { 
        console.log('tag deinit');
        clearTags();
    }

    self.initialize = function() {
        clearTags();
    };

    self.getTags            = getTags;
    self.createTag          = createTag;
    self.clearTags          = clearTags;
    self.isTagsLoaded       = isTagsLoaded;
    self.getTagsIndexedById = getTagsIndexedById;

    return self;
})();