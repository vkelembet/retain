app.user = (function() {  // jshint ignore:line
    'use strict';

    var self = {
        $form: undefined,
        $username: undefined,
        $password: undefined,
        $userDetails: undefined,

        details: { },

        callbacks: {
            onSignOutStarted: function(userDetails) {},
            onSignedOut: function() {},
            onSignOutFailure: function(data) {},

            onSignIn: function() {}
        }
    };

    function clearUserDetails() {
        self.details = {};
        self.token = '';
    }

    function clearForm() {
        self.$username.val("");
        self.$password.val("");
    }

    function signInSuccess(data) {
        clearForm();
        populateUserDetails(self.details);

        // Notify subscriber.
        self.callbacks.onSignIn();
    }

    function signInFailure(data) {
        app.hideActivityIndicator();
        
        //app.showNotification('Unable to sign in', JSON.stringify(data));
        app.showNotification("Unable to sign in", 
            "Something went wrong and we can't sign you in right now. " +
            "Please check your credentials and try again later.");
    }

    function validationSuccess(credentials) { 
        app.showActivityIndicator();
        signIn(credentials, signInSuccess, signInFailure);
    }

    function validationFailure(title, message) {
        app.showNotification(title, message);
    }

    function checkSignInResponse(data, success, failure) {
        if (typeof data !== 'undefined') {
            var resp = data.data;

            if (typeof resp !== 'undefined' &&
                resp.success == true && 
                resp.access_token != '' &&
                resp.user !== 'undefined') {
                    self.details    = resp.user;
                    self.token      = resp.access_token;
                    success(data);
            } else {
                failure(data);
            }
        } else {
            failure(data);
        }
    }

    function validate(credentials, success, failure) {
        if (typeof credentials !== 'undefined') {
            var username = credentials.username || '',
                password = credentials.password || '';

            // TODO: better check for space characters
            if (username.length > 0 && password.length > 0) {
                success(credentials);
            } else {
                failure('Sign in error', 'Please enter both Username (Email) and Password!');
            }
        } else {
            failure('Reference error', 'Undefined credentials!');
        }
    }

    function signIn(credentials, success, failure) {
        clearUserDetails();

        if (app.mode.mocked) {
            var resp = {
                "success": true,
                "access_token": "iGtdHnDQkdyZPJHR780-as_wWMSYzoBm",
                "user": {
                    "id": 1,
                    "userName": "admin",
                    "email": "admin@domain.com"
                }
            };

            success(resp);

            return;
        }

        var data = $.param( { 
            email: credentials.username, 
            password: credentials.password 
        } );

        var env = app.environment,
            url = env.rootURL + '/' + env.loginPath;
        
        var posting = $.post( url, data );

        posting.done(function(data) {
            checkSignInResponse(data, success, failure);
        });

        posting.fail(function(data) {
            failure(data);
        });
    }

    function signOutSuccess() {
        self.callbacks.onSignedOut();
    }

    function signOutFailure(data) {
        self.callbacks.onSignOutFailure(data);
    }

    function checkSignOutResponse(data, success, failure) {
        if (typeof data !== 'undefined' && data.success == true) {
            success();
            return;
        }

        failure(data);
    }

    function signOut() {
        self.callbacks.onSignOutStarted(self.details);

        var success = signOutSuccess,
            failure = signOutFailure;

        if (typeof self.token !== 'undefined' && self.token != '') {
            var env = app.environment,
                url = env.rootURL + '/' + env.logoutPath,
                authorization = 'Bearer ' + app.user.token;
            
            var posting = $.ajax({ 
                method: 'post',
                url: url,
                headers: { 'Authorization': authorization },
                crossDomain: true
            });

            posting.done(function(data) {
                checkSignOutResponse(data, success, failure);
            });

            posting.fail(function(data) {
                failure(data);
            });
        } 
        
        clearUserDetails();
    }

    function getInitials(details) {
        var username = details.userName,
            words = username.split(' '),
            len = words.length,
            uppers = [];

        for (var i = 0; i < len; i++) {
            uppers.push(words[i].charAt(0).toUpperCase());
        }

        return uppers.join('');
    }

    function populateUserDetails(details) {
        var $details = $(app.selectors.userDetails),
            $initials = $details.find('.ms-Persona-initials'),
            $primary = $details.find('.ms-Persona-primaryText'),
            $email = $details.find('.email'),
            initials = getInitials(details);

        $initials.text(initials);
        $primary.text(details.userName);
        $email.text(details.email);
    }

    function initFormEventHandlers() {
        self.$form.submit( 
            wrapErrors(function(e) {
                // "this" is a reference to the submitted form.
                e.preventDefault();

                var credentials = {
                    username: self.$username.val(),
                    password: self.$password.val()
                };

                validate(credentials, validationSuccess, validationFailure);
            })
        );
    }

    function initSignOutLinkEventHandlers() {
        var $signOutLink = self.$userDetails.find('.sign-out');
        $signOutLink.click(wrapErrors(function() {
            signOut();
        }));
    }

    function initPersonaImageAreaEventHandlers() {
        if (!app.mode.debug) { return; }

        var $imageArea = self.$userDetails.find('.ms-Persona-imageArea');
        $imageArea.click(wrapErrors(function() {
            app.showLog();
        }));
    }

    function initEventHandlers() {
        initFormEventHandlers();
        initSignOutLinkEventHandlers();
        initPersonaImageAreaEventHandlers();
    }

    self.initialize = function() {
        self.$form          = $(app.selectors.signInForm);
        self.$username      = self.$form.find('#username'),
        self.$password      = self.$form.find('#password');
        self.$userDetails   = $(app.selectors.userDetails);

        initEventHandlers();
    };

    self.signOut = signOut;

    return self;
})();