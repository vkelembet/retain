/*!
 * jQuery plugin to present rich selected text options popover
 * Author: Volodymyr Kelembet
 * Licensed under the MIT license
 */


(function ($) {

  if (!$.fn.popover) throw new Error('Page plugin requires extended Bootstrap popover.js');
  if (!$.fn.page) throw new Error('Page plugin requires Bootstrap page.js');

  // PAGE CLASS DEFINITION
  // ====================

  var Highlighter = function ($element, options) {
    this.$element = $element;
    this.options  = $.extend(true, {}, Highlighter.DEFAULTS, options);
  }

  Highlighter.VERSION = '0.0.1';

  Highlighter.TRANSITION_DURATION = 150;

  Highlighter.DEFAULTS = {
    autocompleteSets: [],

    popover: {
      html:       true,
      trigger:    'focus',
      container:  'body',
      placement:  'top',
      title: function() { return ''; },
      content: function() { return ''; } // e.g. $('#template-highlighter').html()
    },

    page: {
      selector: '.page-content'
    },

    datetime: {
      selector: '.date-time-picker'
    },

    tags: {
      template: '<button type="button" class="btn btn-primary btn-tag btn-tag-title" data-toggle="button" aria-pressed="false" autocomplete="off"></button>',
      noTagsTemplate: '<p class="center-text">There is nothing to select!</p><p><a href="#" data-toggle="page" data-target="#page-add-tag">Add new tag</a></p>', 
      selected: undefined
    }
  };

  Highlighter.prototype.show = function () {
    var $this = this.$element;

    // Initialize bs popover.
    $this.popover(this.options.popover);

    // Initialize bs page (when the popover template has been added to the DOM).
    $this.on('inserted.bs.popover', $.proxy(initPages, this) );
    $this.popover('show');
  };

  Highlighter.prototype.hide = function () {
    var $this = this.$element;

    $this.popover('hide');
  };

  Highlighter.prototype.destroy = function () {
    var $this = this.$element;

    deinitAutocompletePlugin.call(this);
    deinitDateTimePlugin.call(this);

    $this.popover('destroy');
  };

  Highlighter.prototype.renderAddNotePage = function (params) {
    // TODO: this.$element.data to this.$pages
    var selectedTag   = params.selectedTag,
        selectedDate  = params.selectedDate,
        $tip = this.$element.data('bs.popover').tip();

    // Show tag title if selected or cleat input.
    var title = '';
    if (typeof selectedTag !== 'undefined') { 
      title = selectedTag.title;
    }
    $tip.find('.input-selected-tag').val(title);

    // Show date if selected of clear input.
    var date = '';
    if (typeof selectedDate !== 'undefined') { 
      date = selectedDate.format('MMMM Do YYYY');
    }
    $tip.find('.input-selected-date').val(date);
  };

  // params.tags: [{ "id": 7, "title": "tag" }, ...]
  // params.selectedTag: { "id": 7, "title": "tag" }
  Highlighter.prototype.renderPickTagPage = function (params) {
    var tags        = params.tags, 
        selectedTag = params.selectedTag,
        $this       = this.$element,
        $pages      = this.$pages;

    
    var len = tags.length, i, $tag,
        $container = $(document.createElement("div"));

    if (len > 0) {
      var tagTemplate = this.options.tags.template;
      
      for (i = 0; i < len; ++i) {
        tag = tags[i];
        $tag = $(tagTemplate);
        
        $tag.find('.btn-tag')
          .addBack('.btn-tag')
          .attr('data-id', tag.id)
          .attr('data-title', tag.title)
          .find('.btn-tag-title')
          .addBack('.btn-tag-title')
          .text(tag.title);

        $container.append($tag);
      }
    } else {
      var noTagsTemplate = this.options.tags.noTagsTemplate;
      $container.append(noTagsTemplate);
    }

    // Add tags to page.
    var $tagContainer = $pages.find('.tag-container');
    $tagContainer.empty().append($container.contents());

    // Toggle selected tag.
    if (typeof selectedTag !== 'undefined') {
      var $selTag = $tagContainer.find('*[data-id="' + selectedTag.id + '"]');
      $selTag.button('toggle');
    }

    // Update popover placement.
    updatePopoverPlacement.call(this);
  };

  Highlighter.prototype.state = function(params) {
    var $this   = this.$element,
        $pages  = $(this.options.page.selector),
        state   = params.state;

    var $btnAddHighlight    = $pages.find('.btn-add-highlight'),
        $btnAddTag          = $pages.find('.btn-add-tag'),
        $inputNewTag        = $pages.find('.input-new-tag'),
        $btnAddNote         = $pages.find('.btn-add-note'),
        $inputNewNoteText   = $pages.find('.input-new-note-text'),
        $inputNewNoteParas  = $pages.find('.input-new-note-paras');

    switch (state) {
      case 'loading-tags':
        break;

      case 'loading-tag-failure':
        $pages
          .find('.tag-container')
          .empty()
          .append('<p class="bg-danger">Tag cloud loading error!</p>');
        break;

      case 'adding-tag':
        setButtonPerformingState($btnAddTag);
        break;

      case 'adding-tag-success':
        setButtonSuccessState($btnAddTag);
        break;

      case 'adding-tag-failure':
        setButtonFailureState($btnAddTag);
        break;

      case 'adding-note':
        setButtonPerformingState($btnAddNote);
        break;

      case 'adding-note-success':
        setButtonSuccessState($btnAddNote);
        break;

      case 'adding-note-failure':
        setButtonFailureState($btnAddNote);
        break;

      case 'adding-highlight-note':
        setButtonPerformingState($btnAddHighlight);
        break;

      case 'adding-highlight-note-success':
        setButtonSuccessState($btnAddHighlight);
        break;

      case 'adding-highlight-note-failure':
        setButtonFailureState($btnAddHighlight);
        break;

      case 'reset':
        resetButtonState($btnAddTag);
        resetButtonState($btnAddNote);
        resetButtonState($btnAddHighlight);

        resetInputState($inputNewTag);
        resetInputState($inputNewNoteText);
        resetInputState($inputNewNoteParas);
        break;

      default:
        throw new Error('Unexpected state: [' + state + ']');
    }

  };

  function setButtonPerformingState($btn) {
      $btn
        .button('performing')
        .prop('disabled', true);
  }

  function setButtonSuccessState($btn) {
      $btn
        .button('success')
        .prop('disabled', true)
        .removeClass('btn-default')
        .addClass('btn-success');
  }

  function setButtonFailureState($btn) {
      $btn
        .button('failure')
        .prop('disabled', true)
        .removeClass('btn-default')
        .addClass('btn-danger');
  }

  function resetButtonState($btn) {
    if (typeof $btn !== 'undefined' && $btn.length > 0) {
      $btn
        .button('reset')
        .prop('disabled', false)
        .addClass('btn-default')
        .removeClass('btn-success btn-danger');
    }
  }

  function clearInput($input) {
    $input.val('');
  }

  function resetInputState($input) {
    $input.val('');
    $input.prop('disabled', false);
  }

  // Init plugins and add event handlers.
  function initPages() {
    var $this   = this.$element,
        $pages  = $(this.options.page.selector);

    this.$pages = $pages;

    // Init plugins.
    this.$pages.page();
    initDateTimePlugin.call(this);
    initAutocompletePlugin.call(this);

    // Init event handlers.
    $(window).resize($.proxy(updatePopoverPlacement, this));
    $pages.on('shown.bs.page', $.proxy(updatePopoverPlacement, this) );
    
    // Notify client on opened page.
    $pages.on('shown.bs.page', function(e) {
      var name = 'shown.highlighter.page',
          shownPageEvent = createShownEvent(e, name);
      $this.trigger(shownPageEvent);

      if (e.target.id == 'page-pick-tag') {
        var name = 'shown.highlighter.pick-tag.page',
            shownPickTagPageEvent = createShownEvent(e, name);
        $this.trigger(shownPickTagPageEvent);
      }

      if (e.target.id == 'page-pick-tag') {
        var name = 'shown.highlighter.add-note.page',
            shownAddNotePageEvent = createShownEvent(e, name);
        $this.trigger(shownAddNotePageEvent);
      }
    });

    // Init handlers for all pages.
    initMainPageHandlers.call(this);
    initAddNotePageHandlers.call(this);
    initPickTagPageHandlers.call(this);
    initAddTagPageHandlers.call(this);
    initPickDatePageHandlers.call(this);
  };

  function initMainPageHandlers() {
    var $this = this.$element,
        $pages = this.$pages;

    // Notify client on insertion button click.
    $pages.on('click', '.btn-insert', function(e) {
      var $btn = $(e.currentTarget),
          eventName = 'clicked.highlighter.insert.btn';

      var clickEvent = $.Event(eventName, {
        target:     $btn,
        action:     $btn.attr('data-action'),
        paragraphs: $pages.find('.input-paras').val()
      });
      
      $this.trigger(clickEvent);
    });

    // Notify client on "Highlight" button click.
    $pages.on('click', '.btn-add-highlight', function(e) {
      var $btn = $(e.currentTarget),
          eventName = 'clicked.highlighter.add-highlight.btn';

      var clickEvent = $.Event(eventName, {
        target: $btn
      });
      
      $this.trigger(clickEvent);      
    });
  }

  function initAddNotePageHandlers() {
    var $this = this.$element,
        $pages = this.$pages;

    // Clear selected tag input.
    $pages.on('click', '.btn-clear-tag', function(e) {
      var $btn    = $(e.currentTarget),
          $input  = $pages.find('.input-selected-tag');

      clearInput($input);  
      $this.trigger($.Event('deselected.highlighter.tag', {
        target: $btn
      }));
    });

    // Clear selected date input.
    $pages.on('click', '.btn-clear-date', function(e) {
      var $btn    = $(e.currentTarget),
          $input  = $pages.find('.input-selected-date');

      clearInput($input); 
      $this.trigger($.Event('deselected.highlighter.date', {
        target: $btn
      }));
    });

    // Notify on "Add Note" button click.
    $pages.on('click', '.btn-add-note', function(e) {
      var $noteTextInput  = $pages.find('.input-new-note-text'),
          noteText        = $noteTextInput.val(),
          $parasInput     = $pages.find('.input-new-note-paras'),
          paras           = $parasInput.val();

      $noteTextInput.parent().removeClass('has-error');

      if (noteText !== '') {
        var $btn = $(e.currentTarget),
            eventName = 'clicked.highlighter.add-note.btn';

        var clickEvent = $.Event(eventName, {
          target:   $btn,
          note:     noteText,
          paras:    paras
        });
        
        $this.trigger(clickEvent);
      } else {
        $noteTextInput.parent().addClass('has-error');
      }
    });
  }

  function initAddTagPageHandlers() {
    var $this = this.$element,
        $pages = this.$pages;

    // Notify on "Add Tag" button click.
    $pages.on('click', '.btn-add-tag', function(e) {
      var $input = $pages.find('.input-new-tag'),
          tagTitle = $input.val();

      $input.parent().removeClass('has-error');

      if (tagTitle !== '') {
        var $btn = $(e.currentTarget),
            eventName = 'clicked.highlighter.add-tag.btn';

        var clickEvent = $.Event(eventName, {
          target:   $btn,
          tagTitle: tagTitle
        });
        
        $this.trigger(clickEvent);
      } else {
        $input.parent().addClass('has-error');
      }
    });
  }

  function initPickDatePageHandlers() {
    var $datetime = this.$datetime,
        $pages    = this.$pages,
        $this     = this.$element;

    /*
      e = {
        date,   // date the picker changed to. Type: moment object (clone)
        oldDate // previous date. Type: moment object (clone) or false in the event of a null
      }
    */
    $datetime.on("dp.change", function(e) {
      // Trigger custom event.
      var name = 'selected.highlighter.date';
      var dateSelectedEvent = $.Event(name, {
        date:     e.date,
        oldDate:  e.oldDate
      });
      
      $this.trigger(dateSelectedEvent);

      // Navigate back to "Add Note" page.
      $pages.find('#page-add-note').page('show');
    });
  }

  function initPickTagPageHandlers() {
    var $this = this.$element,
        $pages = this.$pages;

    $pages.find('#page-pick-tag').on("click", ".btn-tag", function() { 
      var $btn = $(this), eventName;
      
      if ($btn.hasClass('active')) {
        eventName = 'deselected.highlighter.tag';
      } else {
        // Imitate radio behaviour. Deselect all other buttons.
        $btn.siblings('.active').button('toggle');
        eventName = 'selected.highlighter.tag';
      }

      var tag = {
        id: $btn.attr('data-id'),
        title: $btn.attr('data-title')
      }

      var e = $.Event(eventName, {
        target: $btn,
        tag: tag
      });

      $this.trigger(e);

      // Navigate back to "Add Note" page.
      $pages.find('#page-add-note').page('show');
    });
  }

  function createShownEvent(e, name) {
      return $.Event(name, {
        target: e.target,               // newly activated page
        relatedTarget: e.relatedTarget  // previous active page
      });
  }

  function initDateTimePlugin() {
    this.$datetime = $(this.options.datetime.selector);
    this.$datetime.datetimepicker({
      inline: true,
      format: "MM/dd/YYYY"
    });
  }

  function deinitDateTimePlugin() {
      this.$datetime = $(this.options.datetime.selector);
      this.$datetime.datetimepicker('destroy'); 
  }

  function initAutocompletePlugin() {
    var $noteTextArea = this.$pages.find('.input-new-note-text');
    $noteTextArea.textcomplete(this.options.autocompleteSets);
  }

  function deinitAutocompletePlugin() {
    var $pages = this.$pages;
    if ($pages.length > 0) {
      var $noteTextArea = $pages.find('.input-new-note-text');
      $noteTextArea.textcomplete('destroy');
    }
  }

  function updatePopoverPlacement() {
    var $trigger = this.$element;
    if (typeof $trigger !== 'undefined') {
      $trigger.popover('update');
    }
  }

  // HIGHLIGHTER PLUGIN DEFINITION
  // =====================

  function Plugin(action, params) {
    return this.each(function () {
      var $this = $(this);
      var data  = $this.data('bs.highlighter');
      var options = typeof action == 'object' && action;

      if (!data) {
        $this.data('bs.highlighter', ( data = new Highlighter($this, options) ));
      }

      if (typeof action == 'string') {
        if (typeof params == 'object') {
          data[action](params);
        } else {
          data[action]();
        }
      }
    });
  }

  var old = $.fn.highlighter;

  $.fn.highlighter = Plugin;
  $.fn.highlighter.Constructor = Highlighter;


  // HIGHLIGHTER NO CONFLICT
  // ===============

  $.fn.highlighter.noConflict = function () {
    $.fn.highlighter = old;
    return this;
  }

})(jQuery);

