/**
 * Table Row As Radio Selector Plugin
 *
 * Adds ability to select a table row.
 *
 * @param  {jQuery Object}  One or more .ms-Table-row components
 * @return {jQuery Object}  The same components (allows for chaining)
 */
(function ($) {
  $.fn.RadioTableRow = function () {

    /** Go through each row */
    return this.each(function () {

      var $tableRow = $(this);

      /** Detect clicks on selectable list items. */
      $tableRow.on('click', function(event) {
          // Deselect sibling table rows.
          $(this).siblings('.ms-Table-row').removeClass('is-selected');

          // Select current table row.
          $(this).addClass('is-selected');
      });

    });

  };
})(jQuery);